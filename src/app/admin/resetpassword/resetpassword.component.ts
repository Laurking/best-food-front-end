import { Component, OnInit, ɵConsole, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Base64 from 'base-64';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit,OnDestroy {
  public successfullRequest:boolean = false;
  updatePasswordForm:FormGroup;
  public authSubs:Subscription;
  public tokenSub:Subscription;
  public userId:any;
  requestStatus:string = "Sending ...";
    constructor(public router:ActivatedRoute,
    public navigator:Router,
    public authService:AuthService,
    public flashMessageService:FlashMessagesService
    ) { }
  
  ngOnInit() {
    this.router.params.subscribe(params => {
      this.tokenSub = this.authService.validateToken(params.token).subscribe(response =>{
       const token = response.user;
       if(!token){
        this.navigator.navigate(['/africanamericanbestfood/admin/forgot-password'])
        this.flashMessageService.show('Invalid reset token. Please request a new token',{cssClass:'alert-danger',timeout:3000});
      }
      if(new Date(token.resetPasswordToken.expiresAt).getTime() <= new Date().getTime()){
        this.navigator.navigate(['/africanamericanbestfood/admin/forgot-password'])
        this.flashMessageService.show('your reset token has expired',{cssClass:'alert-danger',timeout:3000});
      }
      if(token){
        this.userId = response.user._id;
      }
      },error =>{
        console.log(error);
      })
    });
    
    
    this.updatePasswordForm = this.createUpdatePasswordForm();
  }
  
  ngOnDestroy(){
    if(this.authSubs){
      this.authSubs.unsubscribe();
    }
    if(this.tokenSub){
      this.tokenSub.unsubscribe();
    }
  }
  createUpdatePasswordForm(){
    return new FormGroup({
      newPassword: new FormControl(null, [Validators.required, Validators.minLength(8),Validators.maxLength(20)]),
      confirmPassword: new FormControl(null, [Validators.required])
    });
  
  }
  
  changePassword(){
    if(this.updatePasswordForm.invalid){
      return;
    }
    this.successfullRequest = true;
    const credentials = {
      password:this.updatePasswordForm.value.newPassword,
      confirmPassword:this.updatePasswordForm.value.confirmPassword
    }
    this.authSubs = this.authService.resetPassword(credentials,this.userId).subscribe(response =>{
      this.navigator.navigate(['/africanamericanbestfood/admin/auth/signin'])
      this.flashMessageService.show('Password has been reset. Please login',{cssClass:'alert-success',timeout:3000}) // neet to send email as well
    },error =>{
      console.log(error);
      this.requestStatus = "Failed"
      this.flashMessageService.show("Oops server error! Please try again later",{cssClass:'alert-danger',timeout:3000});
      setTimeout(()=>{this.successfullRequest = false;},3000);
    })
  }



  get newPassword(){
    return this.updatePasswordForm.get('newPassword');
  }
  get confirmPassword(){
    return this.updatePasswordForm.get('confirmPassword');
  }
}



class Crypto{
  static decrypt(message){
   const result = Base64.decode(message);
   return result;
  }
}


