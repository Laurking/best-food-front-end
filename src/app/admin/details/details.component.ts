import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuOutput } from 'src/app/models/menu.model';
import { Subscription } from 'rxjs';
import { MenuService } from 'src/app/services/menu.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {
   menuId:any= null;
   menu:MenuOutput;
   isLoading:boolean = true;
   menuItemSub:Subscription;
  constructor(public menuService:MenuService, public router:ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.menuId = params.id;
    });
    this.menuItemSub = this.menuService.getMenuItem(this.menuId).subscribe(response =>{
      this.menu = response.data;
      this.isLoading = false;
    })
  }

  ngOnDestroy(){
    this.menuItemSub.unsubscribe();
  }


}
