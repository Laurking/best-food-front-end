import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { FlashMessagesModule } from 'angular2-flash-messages';

import { AuthService } from '../services/auth.service';
import { AccountsService } from '../services/accounts.service';
import { ContactService } from '../services/contact.service';
import { MenuService } from '../services/menu.service';
import { ProfileService } from '../services/profile.service';



import { AdminRoutingModule } from './admin-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from '../shared-features/shared.module';


import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { AddMenuComponent } from './add-menu/add-menu.component';
import { DetailsComponent } from './details/details.component';
import { ForgotPasswordComponent } from './forgot-pasword/forgot-password.component';
import { FooterComponent } from './footer/footer.component';
import { AccountComponent } from './account/account.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { ConfirmEqualValidatorDirective } from '../validators/confirm-equal-validator';


import { UpdateMenuComponent } from './update-menu/update-menu.component';
import { DeleteMenuComponent } from './delete-menu/delete-menu.component';
import { AuthInterceptor } from '../utils/auth-interceptor';
import { SettingComponent } from './setting/setting.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFound404Component } from './page-not-found404/page-not-found404.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';










@NgModule({
  declarations: [
    AdminComponent,
    NavbarComponent,
    HomeComponent,
    AuthComponent,
    SigninComponent,
    SignupComponent,
    MenuListComponent,
    AddMenuComponent,
    DetailsComponent,
    ForgotPasswordComponent,
    FooterComponent,
    AccountComponent,
    CreateUserComponent,
    ManageAccountComponent,
    ConfirmEqualValidatorDirective,
    UpdateMenuComponent,
    DeleteMenuComponent,
    SettingComponent,
    ProfileComponent,
    PageNotFound404Component,
    ResetpasswordComponent,
  ],
  providers:[
    AccountsService, 
    AuthService, 
    ContactService, 
    MenuService, 
    ProfileService,
    {provide:HTTP_INTERCEPTORS,
      useClass:AuthInterceptor,
      multi:true
    }
  ],

  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    SharedModule,
    FlashMessagesModule.forRoot()
  ],
  bootstrap:[AdminComponent]
})
export class AdminModule { }
