import { Component, OnInit, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { trigger,state,style,transition,animate} from '@angular/animations';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';



@Component({
  selector: 'admin-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('animateSideBar', [
      state('in', style({
        right: '-235px'
      })),
      state('out', style({
        right: '0px'
      })),
      transition('in => out', animate('200ms ease-in')),
      transition('out => in', animate('200ms ease-out'))
    ])
  ]
})
export class NavbarComponent implements OnInit, OnDestroy{

   isAuthenticated:boolean;
   authSubs:Subscription;
   @ViewChild('sidebar') sidebar:ElementRef;
   menuState:string;

  constructor(
    public router:Router,
    public authService:AuthService, 
    public flashMessageService: FlashMessagesService
    ) {}

  ngOnInit(){
    this.menuState = 'in';
    //this.flashMessageService.show('successfully signed in', { cssClass: 'alert-success', timeout: 300000 });
    this.isAuthenticated = this.authService.getIsAuthenticated();
    this.authSubs = this.authService.getAuthStatusListener().subscribe(authentiated =>{
      this.isAuthenticated = authentiated;
    },error =>{
      console.log(error);
    })
  }

  openSidebar() {
    if(this.menuState == 'in'){
      this.menuState = 'out';
    }
  }

  closeSidebar() {
    if(this.menuState == 'out'){
      this.menuState = 'in';
    }
  }

  ngOnDestroy(){
    this.authSubs.unsubscribe();
  }
  logout(){
    let userAction = "logout";
    this.authService.logout(userAction);
  }

  // @HostListener('window:keydown', ['$event', '$event.target'])
  // keyboardInput(event: KeyboardEvent) {
  //   if(event.keyCode == 27){
  //     this.menuState = 'in';
  //   }
  // }
  
  // @HostListener('window:click', ['$event', '$event.target'])
  // mouseEvent(event) {
  //   let targets = ["menuIcon"];
  //   if(event.target.parentElement === null){
  //     this.menuState = 'in';
  //   }
  //   else{
  //     let target = event.target.parentElement.getAttribute('id');
  //     if(target == null || (target != null && targets.indexOf(target)!=0)){
  //       this.menuState = 'in';
  //     }
  //   }
  // }
}
