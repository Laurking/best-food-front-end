import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  navLinks=[
    {path:'/africanamericanbestfood/admin/create-user',label:'Create User'},
    {path:'/africanamericanbestfood/admin/manage-account',label:'Manage Accounts'}
  ]

  constructor() { }

  ngOnInit() {
  }

}
