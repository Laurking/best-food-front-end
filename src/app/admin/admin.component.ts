import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import {Router, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
  authSub:Subscription;
  previousUrl:string;
  
  constructor(private authService:AuthService,private router:Router) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
  ).subscribe(event => {
      // console.log(event);
    });
  }
    
    ngOnInit() {
      this.authService.autoAuthUser();
    }
    
    ngOnDestroy(){
      
    }
    
  }
  