import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuOutput } from 'src/app/models/menu.model';
import { Subscription } from 'rxjs';
import { MenuService } from 'src/app/services/menu.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { timeInterval } from 'rxjs/operators';


@Component({
  selector: 'app-delete-menu',
  templateUrl: './delete-menu.component.html',
  styleUrls: ['./delete-menu.component.scss']
})
export class DeleteMenuComponent implements OnInit, OnDestroy {
   menuId:any= null;
   menu:MenuOutput;
   isLoading:boolean = true;
   menuItemSub:Subscription;
   deleteMenuSub:Subscription;
   sendingRequest:boolean = false;
  constructor(public menuService:MenuService, 
    public router:ActivatedRoute,
    public navigator:Router,
    public flashMessageService:FlashMessagesService) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.menuId = params.id;
    });
    this.menuItemSub = this.menuService.getMenuItem(this.menuId).subscribe(response =>{
      this.menu = response.data;
      this.isLoading = false;
    })
  }

  deleteMenuItem(menuId:number){
    this.sendingRequest = true;
    this.deleteMenuSub = this.menuService.deleteMenu(menuId).subscribe(response =>{
      this.sendingRequest = false;
      this.flashMessageService.show('Menu successfully deleted', { cssClass: 'alert-success', timeout: 3000 });
      this.navigator.navigate(['/africanamericanbestfood/admin/show-menu']);
    },error =>{
      console.log(error);
      this.flashMessageService.show("Oops server error! Please try again later",{cssClass:'alert-danger',timeout:3000});
      setTimeout(()=>{this.sendingRequest = false;},3000);
      
      
      
    })
  }
  ngOnDestroy(){
    if(this.deleteMenuSub){
      this.deleteMenuSub.unsubscribe();
    }
    if(this.menuItemSub){
      this.menuItemSub.unsubscribe();
    }
  }
}
