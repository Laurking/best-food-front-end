import { Component, OnInit, ViewChild, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MenuService } from 'src/app/services/menu.service';
import { MenuInput } from 'src/app/models/menu.model';
import { FoodCategory } from 'src/app/models/food-category.model';
import { Food } from 'src/app/shared-features/variables/constant';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit,OnDestroy {
  @ViewChild('imgFileInput') imgFileInput :ElementRef;
  @ViewChild('imagePreview') imagePreview:ElementRef;
  categories:FoodCategory[] =[];
  addMenuForm:FormGroup;
  invalidFileInput:boolean = false;
  previewSourceUrl:any;
  picture:File;
  addMenuSub:Subscription;
  sendingRequest:boolean = false;
  serverErrors:ServerError[] = [];
  errorMessage:any = null;
  constructor(
    public router:Router,
    public renderer:Renderer2,
    public flashMessageService:FlashMessagesService, 
    public menuService:MenuService) { }
  
  ngOnInit() {
    this.categories = Food.getCategories();
    this.addMenuForm = this.createAddMenuForm();
  }
  
  ngOnDestroy(){
    if(this.addMenuSub){
      this.addMenuSub.unsubscribe();
    }
  }
  createAddMenuForm(){
    return new FormGroup({
      name: new FormControl("",[Validators.required, Validators.minLength(2), Validators.maxLength(50)]),
      description: new FormControl("",[Validators.required, Validators.minLength(10), Validators.maxLength(500)]),
      price: new FormControl("",[Validators.required, Validators.pattern(/^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/i)]),
      category: new FormControl("",[Validators.required])
    });
  }
  
  onSubmit(){
    this.validateFileInput();
    if(this.invalidFileInput  || this.addMenuForm.invalid){
      return;
    }
    const menu: MenuInput= {
      name:this.addMenuForm.value.name,
      price:this.addMenuForm.value.price,
      category:this.addMenuForm.value.category,
      description: this.addMenuForm.value.description,
      image: this.picture
    }
    this.sendingRequest = true;
    this.addMenuSub = this.menuService.addMenu(menu).subscribe(data=>{
      this.sendingRequest = false;
      this.router.navigate(['/africanamericanbestfood/admin/show-menu']);
      this.flashMessageService.show('menu successfully created',{cssClass:'alert-success',timeout:3000});
    },error =>{
      this.sendingRequest = false;
      this.handleError(error);
    });
  }
  
  
  previewImage(){
    const reader = new FileReader();
    reader.onload = (()=>{
      this.previewSourceUrl = reader.result;
    });
    reader.readAsDataURL(this.picture);
    this.renderer.setStyle(this.imagePreview.nativeElement,'display','block');
  }
  
  closePreview(){
    this.renderer.setStyle(this.imagePreview.nativeElement,'display','none');
  }
  
  onUploadImage($event){
    let file =  $event.target.files[0];
    if(!file){
      this.invalidFileInput = true;
      this.picture=null;
      this.addMenuForm.setErrors({ 'invalid': true });
    }
    else{
      this.invalidFileInput = false;
      this.picture= file;
    }
  }
  
  validateFileInput(){
    let file = this.imgFileInput.nativeElement.files
    if(file.length > 0){
      this.invalidFileInput = false;
      this.picture = file[0];
      
    }
    else{
      this.invalidFileInput = true;
      this.addMenuForm.setErrors({ 'invalid': true });
    }
  }
  
  get name(){
    return this.addMenuForm.get('name');
  }
  get price(){
    return this.addMenuForm.get('price');
  }
  get category(){
    return this.addMenuForm.get('category');
  }
  
  get description(){
    return this.addMenuForm.get('description');
  }
  
  handleError(error){
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      console.log(this.serverErrors);
      return;
    }
    if(error.status == 500){
      this.errorMessage = "Oops server error! Please try again later";
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
