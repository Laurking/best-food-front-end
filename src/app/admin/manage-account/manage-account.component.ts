import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AccountsService } from 'src/app/services/accounts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.scss']
})
export class ManageAccountComponent implements OnInit, OnDestroy {
  // @ViewChild('confirmDeletionPanel') confirmDeletionPanel:ElementRef;
  isLoading:boolean = true;
   users:User[];
   userSub:Subscription;
  
  constructor(
    public renderer:Renderer2,
    public accountService:AccountsService) { }
  
  ngOnInit() {
    this.accountService.retrieveAllUsers();
    this.userSub = this.accountService.getUsers().subscribe(response =>{
      this.users = response.users
      this.isLoading = false;
    },error =>{
      console.log(error);
    })
  }
  
  ngOnDestroy(){
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }
  
  activateUser(userId:any){
    this.accountService.activateUser(userId);
  }

  deactivateUser(userId:any){
    this.accountService.deactivateUser(userId);
  }

  confirmDeletion(event){
    event.preventDefault();
    const element = event.target.parentNode.parentNode.parentNode.children[1];
    if(element){
      element.classList.add('show');
    }
    // this.renderer.setStyle(event.target.parentNode.parentNode.parentNode.children[1],'display','block');
  }

  deleteUser(userId:any){
    this.accountService.deleteUser(userId);
  }


  cancelUserDeletion(event){
    event.preventDefault();
   const element = event.target.parentNode.parentNode.parentNode.parentNode;
   if(element){
    element.classList.remove('show')
   }
    // this.renderer.setStyle(event.target.parentNode.parentNode.parentNode.parentNode,'display','none');
  }
}
