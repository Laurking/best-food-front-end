import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found404',
  templateUrl: './page-not-found404.component.html',
  styleUrls: ['./page-not-found404.component.scss']
})
export class PageNotFound404Component implements OnInit {
  
  public href:string=null;
  constructor(public router:Router) { }

  ngOnInit() {
    this.href = this.router.url;
  }


}
