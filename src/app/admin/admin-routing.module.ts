import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminAuthenticationGuard,
         AdminAuthorizationGuard} from '../authugards/admin-auth.guard';

import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { AddMenuComponent } from './add-menu/add-menu.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-pasword/forgot-password.component';
import { AccountComponent } from './account/account.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { DetailsComponent } from './details/details.component';
import { UpdateMenuComponent } from './update-menu/update-menu.component';
import { DeleteMenuComponent } from './delete-menu/delete-menu.component';
import { SettingComponent } from './setting/setting.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFound404Component } from './page-not-found404/page-not-found404.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';





const routes: Routes = [
    { path:'africanamericanbestfood/admin',
    component:AdminComponent,
    children:[
        { path:'',
        component:HomeComponent
    },
    
    { path:'add-menu',
    component: AddMenuComponent, canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard]
},

{   path:'show-menu',
component: MenuListComponent,canActivate:[AdminAuthenticationGuard]
},

{   path:'details/:id',
component: DetailsComponent,canActivate:[AdminAuthenticationGuard]
},

{   path:'update/:id',
component: UpdateMenuComponent,canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard]
},

{   path:'remove/:id',
component: DeleteMenuComponent,canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard]
},

{
    path:'create-user',
    component: AccountComponent, canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard],
    children:[
        {path:'',component:CreateUserComponent},
    ]
},
{
    path:'manage-account',
    component: AccountComponent,canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard],
    children:[
        {path:'',component:ManageAccountComponent}
    ]
},

{ path: 'forgot-password',
component: ForgotPasswordComponent},

{ path: 'change-password/:token',
component: ResetpasswordComponent},

{path:'profile',component: ProfileComponent, canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard]},
    
{path:'setting',component: SettingComponent,canActivate:[AdminAuthenticationGuard,AdminAuthorizationGuard]},

// {path:'auth/signin', component:AuthComponent,
// canActivateChild:[AdminAuthenticationGuard],
// children:[
//     {path:'',component:SigninComponent}
// ]},
{path:'auth',component:AuthComponent, 
// canActivateChild:[AdminUnauthorizedWhenAuthenticatedGuard],
children:[
    {path:'signin',component:SigninComponent},
    {path:'signup',component:SignupComponent}]},
    

    {path:'404',component:PageNotFound404Component},

    {path:'**',redirectTo:'404',pathMatch:'full'}

]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    providers:[AdminAuthorizationGuard,AdminAuthenticationGuard],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
