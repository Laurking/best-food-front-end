import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  signupForm: FormGroup;
  signupSub:Subscription;
  sendingRequest:boolean = false;
  serverErrors:ServerError[] = [];
  errorMessage:any = null;
  constructor(public flashMessageService:FlashMessagesService, public router:Router, public authService:AuthService) { 
  }
  
  ngOnInit() {
    if(this.authService.getIsAuthenticated()){
      this.router.navigate(['/africanamericanbestfood/admin/']);
      this.flashMessageService.show('You are already authenticated', { cssClass: 'alert-info', timeout: 3000 });
    }
    this.signupForm = this.createSignupForm();
  }
  
  createSignupForm() {
    return new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.pattern(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8),Validators.maxLength(20)]),
      confirmPassword: new FormControl(null, [Validators.required])
    });
  }
  
  onSubmit(){
    this.errorMessage = '';
    if(this.signupForm.invalid){
      return;
    }
    const signupDetails={
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
      confirmPassword: this.signupForm.value.confirmPassword
    }
    this.sendingRequest = true;
    this.signupSub =  this.authService.signup(signupDetails).subscribe(response =>{
      this.sendingRequest = false;
      this.router.navigate(['africanamericanbestfood/admin/auth/signin'])
      this.flashMessageService.show('successfully signed up', { cssClass: 'alert-success', timeout: 3000 });
    },error =>{
      this.sendingRequest = false;
      this.handleError(error);
    });
  }
  
  ngOnDestroy(){
    if(this.signupSub){
      this.signupSub.unsubscribe();  
    }
  }
  
  revert() {
    this.signupForm.reset();
  }
  
  get email(){
    return this.signupForm.get('email');
  }
  get password(){
    return this.signupForm.get('password');
  }
  get confirmPassword(){
    return this.signupForm.get('confirmPassword');
  }
  
  handleError(error){
    if(error.status == 401){
      this.errorMessage = error.error.message;
      return;
    }
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      if(this.serverErrors && this.serverErrors[0].msg == 'email already exists'){
        this.errorMessage = this.serverErrors[0].msg;
        return;
      }
      return;
    }
    if(error.status == 500){
      this.errorMessage = "Server error. Please try again later";
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
