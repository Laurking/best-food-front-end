import { Component, OnInit, ViewChild, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { AccountsService } from 'src/app/services/accounts.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  
  @ViewChild('updatePannel') updatePannel:ElementRef;
  @ViewChild('passwordForm') passwordForm:any;
  updatePasswordForm:FormGroup;
  updateProfileForm:FormGroup;
  user:User;
  authSub:Subscription;
  serverErrors:ServerError[] = [];
  errorMessage:any = null;
  sendingRequest:boolean = false;
  constructor(public renderer:Renderer2,
    public flashMessageService:FlashMessagesService,
    public accountsService:AccountsService,
    public authService:AuthService) { }
    
    ngOnInit() {
      this.user = this.authService.getUser();
      this.updateProfileForm = this.createUpdateProfileInfoForm();
      this.updatePasswordForm = this.createUpdatePasswordForm();
    }
    
    ngOnDestroy(){
      if(this.authSub){
        this.authSub.unsubscribe();
      }
    }
    
    
    updatePersonalInfo(){
      this.serverErrors = [];
      this.errorMessage = "";
      if(this.updateProfileForm.invalid){
        return;
      }
      if(!this.isOverEighteen(new Date(this.updateProfileForm.value.birthdate))){
        this.updateProfileForm.controls['birthdate'].setErrors({'invalid': true});
        return;
      }
      const userData ={
        userId:this.user._id,
        firstname:this.updateProfileForm.value.firstname,
        lastname:this.updateProfileForm.value.lastname,
        birthDate:this.updateProfileForm.value.birthdate
      };
      if(this.equals(userData,this.user)){
        this.renderer.setStyle(this.updatePannel.nativeElement,'display','none');
        return;
      }
      this.sendingRequest = true;
      this.authSub = this.authService.updateCurrentUserInfo(userData).subscribe(response =>{
        this.sendingRequest = false;
        this.user = response.user;
        this.authService.setUser(this.user);
        this.updateProfileForm.reset(this.updateProfileForm.value);
        this.renderer.setStyle(this.updatePannel.nativeElement,'display','none');
        this.flashMessageService.show('Personal Information updated',{cssClass:'alert-success',timeout:3000});
      },error =>{
       this.sendingRequest = false;
        this.handlePersonalInfoUpdateErrors(error);
      })
    }
    
    changePassword(){
      const validNewPassword = this.updatePasswordForm.value.newPassword == this.updatePasswordForm.value.confirmPassword;
      if(this.updatePasswordForm.invalid || !validNewPassword){
        return;
      }
      this.serverErrors = [];
      this.errorMessage = "";
      const passwordData={
        userId: this.user._id,
        currentPassword: this.updatePasswordForm.value.currentPassword,
        newPassword: this.updatePasswordForm.value.newPassword,
        confirmPassword: this.updatePasswordForm.value.confirmPassword
      }
      this.sendingRequest = true;
      this.accountsService.updateUserPassword(passwordData).subscribe(response =>{
        this.user.passwordUpdatedAt = response.passwordUpdatedAt;
        this.authService.setUser(this.user);
        console.log(response);
        this.sendingRequest = false;
        window.scroll(0, 0);
        this.flashMessageService.show('Password successfully updated', { cssClass: 'alert-success', timeout: 3000 });
        this.passwordForm.resetForm();
      },error =>{
        this.sendingRequest = false;
        this.handlePasswordChangeErrors(error);
      })
    }
    
    createUpdatePasswordForm(){
      return new FormGroup({
        currentPassword: new FormControl("", [Validators.required]),
        newPassword: new FormControl("", [Validators.required, Validators.minLength(8),Validators.maxLength(20)]),
        confirmPassword: new FormControl("", [Validators.required])
      });
    }
    
    createUpdateProfileInfoForm(){
      return new FormGroup({
        firstname: new FormControl(this.user.firstname, [Validators.required,Validators.minLength(2),Validators.maxLength(20)]),
        lastname: new FormControl(this.user.lastname, [Validators.required, Validators.minLength(2),Validators.maxLength(20)]),
        birthdate: new FormControl(this.user.birthDate, [Validators.required])
      });
    }
    
    
    
    openUpdatePannel(){
      this.renderer.setStyle(this.updatePannel.nativeElement,'display','flex');
    }
    
    closePannel(){
      this.renderer.setStyle(this.updatePannel.nativeElement,'display','none');
    }
    
    get currentPassword(){
      return this.updatePasswordForm.get('currentPassword');
    }
    get newPassword(){
      return this.updatePasswordForm.get('newPassword');
    }
    get confirmPassword(){
      return this.updatePasswordForm.get('confirmPassword');
    }
    
    get firstname(){
      return this.updateProfileForm.get('firstname');
    }
    
    get lastname(){
      return this.updateProfileForm.get('lastname');
    }
    
    get birthdate(){
      return this.updateProfileForm.get('birthdate');
    }

    isOverEighteen(date:Date) {
      const year = date.getFullYear();
      const month = date.getMonth();
      const day = date.getDay();
      var now = parseInt(new Date().toISOString().slice(0, 10).replace(/-/g, ''));
      var dob = year * 10000 + month * 100 + day * 1; // Coerces strings to integers
      return (now - dob > 150000) && (now - dob < 1000000);
    }


    equals(user1,user2){
      if((user1.firstname === user2.firstname) && 
      (user1.lastname === user2.lastname) &&
        user1.birthDate === user2.birthDate){
          return true;
      }
      return false;
    }
    handlePasswordChangeErrors(error){
      if(error.status == 401){
        this.errorMessage = error.error.message;
        return;
      }
      if(error.status == 422){
        this.serverErrors = new UniqueError().set(error.error.errorData);
        return;
      }
      if(error.status == 500){
        this.errorMessage = "Server error. Please try again later";
      }
      else{
        this.serverErrors=[];
        this.errorMessage=null;
      }
    }

    handlePersonalInfoUpdateErrors(error){
      if(error.status == 422){
        this.serverErrors = new UniqueError().set(error.error.errorData);
        if(this.serverErrors && this.serverErrors[0].msg == 'Your age must be between 15 and 100.'){
          this.errorMessage = this.serverErrors[0].msg;
          return;
        }
        return;
      }
      if(error.status == 500){
        this.errorMessage = "Server error. Please try again later";
      }
      else{
        this.serverErrors=[];
        this.errorMessage=null;
      }
    }
  }
  