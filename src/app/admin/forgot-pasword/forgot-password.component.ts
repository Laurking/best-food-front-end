import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  @ViewChild('confirmation') confirmation:ElementRef;
  successfullRequest:boolean = false;
  forgotPasswordForm:FormGroup;
  public authSub:Subscription;
  serverErrors:ServerError[] = [];
  errorMessage:any = null;
  constructor(
    public renderer:Renderer2,
    public authService:AuthService) { }
  
  ngOnInit() {
    this.forgotPasswordForm = this.createForgotPasswordForm();
  }
  
  createForgotPasswordForm(){
    return new FormGroup({
      email: new FormControl("", [Validators.required,Validators.pattern(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i)])
    });
  }
  
  onSubmit(){
    if(this.forgotPasswordForm.invalid){
      return;
    }
    this.errorMessage = '';
    this.successfullRequest = true;
    this.authSub = this.authService.forgotPassword({email:this.forgotPasswordForm.value.email}).subscribe(response =>{
      this.successfullRequest = false;
      this.renderer.setStyle(this.confirmation.nativeElement,'display','block');
    },error =>{
      this.successfullRequest = false;
      this.handleError(error);
    })
  }

  get email(){
    return this.forgotPasswordForm.get('email');
  }

  handleError(error){
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      if(this.serverErrors && this.serverErrors[0].msg == 'No such an email in our record.'){
        this.errorMessage = this.serverErrors[0].msg;
      }
      return;
    }
    if(error.status == 500){
      this.errorMessage = "Oops server error! Please try again later";
      return;
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }

}
