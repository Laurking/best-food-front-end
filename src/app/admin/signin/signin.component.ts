import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit,OnDestroy {
  
   signinForm: FormGroup;
   errorSubs:Subscription;
   serverErrors:ServerError[] = [];
   errorMessage:string = null;
   sendingRequest:boolean = false;
  constructor(@Inject(ActivatedRoute) private route : ActivatedRoute,
    public router:Router,
    public flashMessageService:FlashMessagesService,
    public authService:AuthService) { }
  
  ngOnInit() {
    if(this.authService.getIsAuthenticated()){
      this.router.navigate(['/africanamericanbestfood/admin/']);
      this.flashMessageService.show('You are already authenticated', { cssClass: 'alert-info', timeout: 3000 });
    }
    this.authService.setActiveRoute(this.route.snapshot.queryParams['redirectTo']);
    this.signinForm = this.createSigninForm();
  }
  
   createSigninForm() {
    return new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.pattern(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i)]),
      password: new FormControl(null, [Validators.required]),
    });
  }
  
  onSubmit(){
    this.errorMessage = '';
    if(this.signinForm.invalid){
      return;
    }
    const signinDetails={
      email: this.signinForm.value.email,
      password: this.signinForm.value.password
    }
    this.sendingRequest = true;
    this.authService.signin(signinDetails);
    this.authService.getAuthStatusListener().subscribe(response =>{
      this.sendingRequest = false;
    });
    this.errorSubs = this.authService.getAuthenticationErrorListener().subscribe(error =>{
      this.sendingRequest = false;
      this.handleError(error);
     });
  }
  
  ngOnDestroy(){
    if(this.errorSubs){
      this.errorSubs.unsubscribe();
    }
  }
  
  revert(){
    this.signinForm.reset();
  }
  
  get email(){
    return this.signinForm.get('email');
  }
  
  get password(){
    return this.signinForm.get('password');
  }
  
  handleError(error){
    if(error.status == 422){
      if(error.error.errorData){
        const errors = error.error.errorData;
        if(errors.length == 1 && errors[0].msg =='user does not exist'){
          this.errorMessage = errors[0].msg;
          return;
        }else{
          this.serverErrors = new UniqueError().set(error.error.errorData);
          return; 
        }
      }else{
        this.errorMessage = error.error.message;
      }
    }else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
