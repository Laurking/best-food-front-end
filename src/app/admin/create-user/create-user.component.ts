import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountsService } from 'src/app/services/accounts.service';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {
   userForm:FormGroup;
   createUserSub:Subscription;
   sendingRequest:boolean = false;
   serverErrors:ServerError[] =[];
   errorMessage:any = null;
   roles=[
    {name:'Owner',displayName:'Owner'},
    {name:'Super Admin',displayName:'Super Admin'},
    {name:'Admin',displayName:'Admin'}
  ]
  constructor(public accountsService:AccountsService,
    public flashMessageService:FlashMessagesService,
    public router:Router
    ) { }

  ngOnInit() {
    this.userForm = this.createUserForm();
  }

   createUserForm(){
    return new FormGroup({
      email: new FormControl(null,[Validators.required,Validators.pattern(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i)]),
      role: new FormControl(null,[Validators.required])
    });
  }

  onSubmit(){
    if(this.userForm.invalid){
      return;
    }
    let newUser={
      email: this.userForm.value.email,
      role:this.userForm.value.role
    }
    this.sendingRequest = true;
    this.createUserSub = this.accountsService.createUser(newUser).subscribe(response =>{
      this.sendingRequest = false;
      this.router.navigate(['/africanamericanbestfood/admin/manage-account']);
      this.flashMessageService.show('Successfully created new user', { cssClass: 'alert-success', timeout: 3000 });
    },error =>{
      this.sendingRequest = false;
      this.handleError(error);
    })
  }

  ngOnDestroy(){
    if(this.createUserSub){
      this.createUserSub.unsubscribe();
    }
  }
  get email(){
    return this.userForm.get('email');
  }

  get role(){
    return this.userForm.get('role');
  }

  handleError(error){
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      if(this.serverErrors && this.serverErrors[0].msg == 'email already exists'){
        this.errorMessage = this.serverErrors[0].msg;
        return;
      }
      return;
    }
    if(error.status == 500){
      this.errorMessage = "Oops server error! Please try again later";
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
