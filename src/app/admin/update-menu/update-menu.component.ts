import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {ActivatedRoute, Router } from '@angular/router';

import { MenuOutput } from 'src/app/models/menu.model';
import { MenuInput } from 'src/app/models/menu.model';
import { FoodCategory } from 'src/app/models/food-category.model';
import { Food } from 'src/app/shared-features/variables/constant';
import { MenuService } from 'src/app/services/menu.service';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-update-menu',
  templateUrl: './update-menu.component.html',
  styleUrls: ['./update-menu.component.scss']
})
export class UpdateMenuComponent implements OnInit {
  @ViewChild('imgFileInput') imgFileInput :ElementRef;
  @ViewChild('imagePreview') imagePreview:ElementRef;
   categories:FoodCategory[] =[];
   updateMenuForm:FormGroup;
   invalidFileInput:boolean = false;
   previewSourceUrl:any;
   picture:File;
   menu:MenuOutput;
   menuId:any= null;
   menuItemSub:Subscription;
   isLoading:boolean=true;
   sendingRequest:boolean = false;
   serverErrors:ServerError[]=[];
   errorMessage:any = null;
  constructor(
    public navigator:Router,
    public menuService:MenuService,
    public renderer:Renderer2, 
    public flashMessageService:FlashMessagesService,
    public router:ActivatedRoute) { }
  
  ngOnInit() {
    this.router.params.subscribe(params => {
      this.menuId = params.id;
    });
    this.menuItemSub = this.menuService.getMenuItem(this.menuId).subscribe(response =>{
      this.menu = response.data;
      this.isLoading = false;
      this.categories = Food.getCategories();
      this.updateMenuForm = this.createUpdateMenuForm();
    })
  }
  
  
   createUpdateMenuForm(){
    return new FormGroup({
      name: new FormControl(this.menu.name,[Validators.required, Validators.minLength(2), Validators.maxLength(50)]),
      price: new FormControl(this.menu.price,[Validators.required, Validators.pattern(/^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/i)]),
      description: new FormControl(this.menu.description,[Validators.required, Validators.minLength(10), Validators.maxLength(500)]),
      category: new FormControl(this.menu.category,[Validators.required])
    });
  }
  
  onSubmit(){
    this.errorMessage = "";
    this.validateFileInput();
    if(this.updateMenuForm.invalid){
      return;
    }
    const menu: MenuInput= {
      name:this.updateMenuForm.value.name,
      price:this.updateMenuForm.value.price,
      category:this.updateMenuForm.value.category,
      description: this.updateMenuForm.value.description,
      image: this.picture
    }
   this.sendingRequest = true;
    this.menuService.updateMenu(this.menuId,menu).subscribe(response=>{
      this.sendingRequest = false;
      this.navigator.navigate(['/africanamericanbestfood/admin/details/'+this.menuId.toString()]);
      this.flashMessageService.show('Menu successfully updated.',{cssClass:'alert-success',timeout:3000});
    },error =>{
      this.sendingRequest = false;
      this.handleError(error);
    })
  }
  
  
  previewNewImage(){
    if(this.picture){
      const reader = new FileReader();
      reader.onload = (()=>{
        this.previewSourceUrl = reader.result;
      });
      reader.readAsDataURL(this.picture);
    }
    this.previewSourceUrl=null;
    this.renderer.setStyle(this.imagePreview.nativeElement,'display','block');
  }
  
  closePreview(){
    this.renderer.setStyle(this.imagePreview.nativeElement,'display','none');
  }
  
  onUploadImage($event){
    let file =  $event.target.files[0];
    if(!file){
      this.invalidFileInput = true;
      this.picture=null;
    }
    else{
      this.invalidFileInput = false;
      this.picture= file;
    }
  }
  
   validateFileInput(){
    let file = this.imgFileInput.nativeElement.files
    if(file.length > 0){
      this.invalidFileInput = false;
      this.picture = file[0];
      
    }
    else{
      this.invalidFileInput = true;
    }
  }
  
  get name(){
    return this.updateMenuForm.get('name');
  }
  get price(){
    return this.updateMenuForm.get('price');
  }
  get category(){
    return this.updateMenuForm.get('category');
  }
  
  get description(){
    return this.updateMenuForm.get('description');
  }
  
  handleError(error){
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      return;
    }
    if(error.status == 500){
      this.errorMessage = "Oops server error! Please try again later";
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
