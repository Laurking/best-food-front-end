import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuOutput } from 'src/app/models/menu.model';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit, OnDestroy {

   foodList: MenuOutput[];
   foodSub;
   isLoading:boolean = true;

  constructor(public menuService:MenuService,public domSanitizer: DomSanitizer) { }

  ngOnInit() {
   this.foodSub =  this.menuService.getMenuList().subscribe(response=>{
      this.foodList = response.data.reverse();
      this.isLoading = false;
    }, error =>{
      console.log(error);
    })
  }


  embedImage(image:string, type:string){
   return this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,'+image);
  }

  ngOnDestroy(){
    this.foodSub.unsubscribe();
  }

}
