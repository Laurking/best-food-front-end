import { Component, OnInit, ViewChild, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import { HomepageOutput, HomepageInput } from 'src/app/models/homepage.model';
import { AboutpageOutput, AboutpageInput } from 'src/app/models/aboutpage.model';
import { SettingService } from 'src/app/services/setting.service';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit, OnDestroy {
  @ViewChild('homeUpdatePanel') homeUpdatePanel:ElementRef;
  @ViewChild('homePagePanel') homePagePanel:ElementRef;
  @ViewChild('aboutUpdatePanel') aboutUpdatePanel:ElementRef;
  @ViewChild('aboutPagePanel') aboutPagePanel:ElementRef;
  sendingUpdateHomepageRequest:boolean = false;
  sendingUpdateAboutpageRequest:boolean = false;
  homepageIsLoading:boolean = true;
  aboutpageIsLoading:boolean = true;
  sendingRequest:boolean = false;
  homePageData:HomepageOutput = null;
  aboutPageData:AboutpageOutput = null;
  HomepageForm:FormGroup;
  aboutpageForm:FormGroup;
  retrieveHomepageDataSub:Subscription;
  retrieveAboutpageDataSub:Subscription;
  updateHomepageSub:Subscription;
  updateAboutpageSub:Subscription;
  
  homeImagePic:File;
  previewHomeImageSourceUrl:any;
  
  aboutTopLeftImagePic:File;
  previewAboutTopLeftSourceUrl:any;
  
  aboutTopRightImagePic:File;
  previewAboutTopRightSourceUrl:any;
  
  aboutBottomLeftImagePic:File;
  previewAboutBottomLeftSourceUrl:any;
  
  aboutBottomCenteredImagePic:File;
  previewAboutBottomCenteredSourceUrl:any;
  
  aboutBottomRightImagePic:File;
  previewAboutBottomRightSourceUrl:any;
  
  constructor(public renderer:Renderer2,
    public settingService:SettingService) { }
    
    ngOnInit() {
      this.retrieveHomepageDataSub = this.settingService.retrieveHomepageData().subscribe(response =>{
        this.homePageData = response.data;
        this.homepageIsLoading = false;
        this.HomepageForm = this.createHomepageForm();
      },error =>{
        console.log(error);
      })
      this.retrieveAboutpageDataSub = this.settingService.retrieveAboutpageData().subscribe(response =>{
        this.aboutPageData = response.data;
        this.aboutpageIsLoading = false;
        this.aboutpageForm = this.createAboutpageForm();
      },error =>{
        console.log(error);
      })
      
      
    }
    
    ngOnDestroy(){
      if(this.retrieveHomepageDataSub){
        this.retrieveHomepageDataSub.unsubscribe();
      }
      if(this.retrieveAboutpageDataSub){
        this.retrieveAboutpageDataSub.unsubscribe();
      }
      if(this.updateHomepageSub){
        this.updateHomepageSub.unsubscribe();
      }
      if(this.updateAboutpageSub){
        this.updateAboutpageSub.unsubscribe();
      }
    }
    
    
    
    
    updateHomePage(){
      if(this.HomepageForm.invalid){
        return;
      }
      const data: HomepageInput ={
        header:this.HomepageForm.value.homeHeader,
        quote:this.HomepageForm.value.homeQuote,
        image: this.homeImagePic
      }
      this.sendingUpdateHomepageRequest = true;
      this.updateHomepageSub = this.settingService.updateHomepageData(data).subscribe(response =>{
        this.homePageData = response.data;
        this.sendingUpdateHomepageRequest = false;
        this.renderer.setStyle(this.homeUpdatePanel.nativeElement,'display','none');
        this.renderer.setStyle(this.homePagePanel.nativeElement,'display','none');
      },error =>{
        this.sendingUpdateHomepageRequest = false;
        console.log(error);
      })
    }
    
    updateAboutPage(){
      if(this.aboutpageForm.invalid){
        return;
      }
      const data: AboutpageInput = {
        header: this.aboutpageForm.value.aboutHeader,
        quote:this.aboutpageForm.value.aboutQuote,
        phone:this.extractNumbers(this.aboutpageForm.value.aboutPhone),
        topLeft: this.aboutTopLeftImagePic,
        topRight: this.aboutTopRightImagePic,
        bottomLeft: this.aboutBottomLeftImagePic,
        bottomCenter: this.aboutBottomCenteredImagePic,
        bottomRight: this.aboutBottomRightImagePic
      }
      this.sendingUpdateAboutpageRequest = true;
      this.updateAboutpageSub = this.settingService.updateAboutpageData(data).subscribe(response =>{
        this.aboutPageData = response.data;
        this.sendingUpdateAboutpageRequest = false;
        this.renderer.setStyle(this.aboutUpdatePanel.nativeElement,'display','none');
        this.renderer.setStyle(this.aboutPagePanel.nativeElement,'display','none');
        window.scrollTo(0,document.body.scrollHeight);
      },error =>{
        this.sendingUpdateAboutpageRequest = false;
        console.log(error);
      })
    }
    
    displayHomeUpdatePanel(){
      this.renderer.setStyle(this.homeUpdatePanel.nativeElement,'display','block');
      this.renderer.setStyle(this.homePagePanel.nativeElement,'display','block');
    }
    
    displayAboutUpdatePanel(){
      this.renderer.setStyle(this.aboutUpdatePanel.nativeElement,'display','block');
      this.renderer.setStyle(this.aboutPagePanel.nativeElement,'display','block');
      window.scrollTo(0,0);
    }
    
    closeAHomeUpdatePanel(){
      this.renderer.setStyle(this.homeUpdatePanel.nativeElement,'display','none');
      this.renderer.setStyle(this.homePagePanel.nativeElement,'display','none');
    }
    
    closeAboutUpdatePanel(){
      this.renderer.setStyle(this.aboutUpdatePanel.nativeElement,'display','none');
      this.renderer.setStyle(this.aboutPagePanel.nativeElement,'display','none');
      window.scrollTo(0,document.body.scrollHeight);
    }
    
    createHomepageForm(){
      return new FormGroup({
        homeHeader: new FormControl(this.homePageData.header, [Validators.required, Validators.minLength(10),Validators.maxLength(50)]),
        homeQuote: new FormControl(this.homePageData.quote, [Validators.required, Validators.minLength(10),Validators.maxLength(50)])
      })
    }
    
    createAboutpageForm(){
      return new FormGroup({
        aboutHeader: new FormControl(this.aboutPageData.header, [Validators.required, Validators.minLength(10),Validators.maxLength(50)]),
        aboutQuote: new FormControl(this.aboutPageData.quote, [Validators.required, Validators.minLength(10),Validators.maxLength(50)]),
        aboutPhone: new FormControl(this.aboutPageData.phone, [Validators.required, Validators.pattern(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/i)])
      })
    }
    
    onUploadHomeImage($event){
      $event.preventDefault();
      let file =  $event.target.files[0];
      if(file){
        this.homeImagePic = file;
        const reader = new FileReader();
        reader.onload = (()=>{
          this.previewHomeImageSourceUrl = reader.result;
        });
        reader.readAsDataURL(this.homeImagePic);
      }
      if(!file){
        this.homeImagePic = null;
        this.previewHomeImageSourceUrl = null;
      }
    }
    
    
    onUploadTopLeftImage($event){
      $event.preventDefault();
      let file =  $event.target.files[0];
      if(file){
        this.aboutTopLeftImagePic = file;
        const reader = new FileReader();
        reader.onload = (()=>{
          this.previewAboutTopLeftSourceUrl = reader.result;
        });
        reader.readAsDataURL(this.aboutTopLeftImagePic);
      }
      if(!file){
        this.aboutTopLeftImagePic = null;
        this.previewAboutTopLeftSourceUrl = null;
      }
    }
    
    onUploadBottomLeftImage($event){
      let file =  $event.target.files[0];
      if(file){
        this.aboutBottomLeftImagePic = file;
        const reader = new FileReader();
        reader.onload = (()=>{
          this.previewAboutBottomLeftSourceUrl = reader.result;
        });
        reader.readAsDataURL(this.aboutBottomLeftImagePic);
      }
      if(!file){
        this.aboutBottomLeftImagePic = null;
        this.previewAboutBottomLeftSourceUrl = null;
      }
    }
    
    onUploadBottomCenteredImage($event){
      let file =  $event.target.files[0];
      if(file){
        this.aboutBottomCenteredImagePic = file;
        const reader = new FileReader();
        reader.onload = (()=>{
          this.previewAboutBottomCenteredSourceUrl = reader.result;
        });
        reader.readAsDataURL(this.aboutBottomCenteredImagePic);
      }
      if(!file){
        this.aboutBottomCenteredImagePic = null;
        this.previewAboutBottomCenteredSourceUrl = null;
      }
    }
    
    onUploadBottomRightImage($event){
      let file =  $event.target.files[0];
      if(file){
        this.aboutBottomRightImagePic = file;
        const reader = new FileReader();
        reader.onload = (()=>{
          this.previewAboutBottomRightSourceUrl = reader.result;
        });
        reader.readAsDataURL(this.aboutBottomRightImagePic);
      }
      if(!file){
        this.aboutBottomRightImagePic = null;
        this.previewAboutBottomRightSourceUrl = null;
      }
    }
    
    extractNumbers(number:string){
      var extracted = number.match(/\d+/g).join("");
      return extracted;
    }
    
    // homepage getters
    get homeHeader(){return this.HomepageForm.get('homeHeader');}
    
    get homeQuote(){return this.HomepageForm.get('homeQuote');}
    
    get homeImage(){return this.HomepageForm.get('homeImage');}
    
    
    // aboutpage getters
    
    get aboutHeader(){return this.aboutpageForm.get("aboutHeader");}
    
    get aboutQuote(){return this.aboutpageForm.get("aboutQuote");}
    
    get aboutPhone(){return this.aboutpageForm.get("aboutPhone");}
    
  }
  