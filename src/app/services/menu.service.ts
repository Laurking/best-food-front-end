import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Subject } from "rxjs";

import { Variables } from '../shared-features/variables/constant';
import { MenuOutput, MenuInput } from '../models/menu.model';


@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private baseUrl = Variables.getBaseUrl();
  private referenceMenu: MenuOutput[];
  private displayedMenu = new Subject<{referenceMenu:MenuOutput[]}>();
  private httpOptions = { headers: new HttpHeaders({
    'Accept':'application/json'
  })
};
constructor(private http:HttpClient, private router:Router) {}

// getMenuList(){
//   return this.http.get<{message:string, data:MenuOutput[]}>(this.baseUrl+'/retrieve-all-menu')
//   .subscribe(response=>{
//     this.referenceMenu = response.data;
//    return this.displayedMenu.next({referenceMenu:[... this.referenceMenu]})
//   });
// }

getMenuList(){
  return this.http.get<{message:string, data: MenuOutput[]}>(this.baseUrl+'/retrieve-all-menu');
}
addMenu(menu:MenuInput){
  const newMenu = new FormData();
  newMenu.append("name", menu.name);
  newMenu.append("price", menu.price);
  newMenu.append("category", menu.category);
  newMenu.append("description", menu.description);
  newMenu.append("image", menu.image, menu.image.name);
  return this.http.post(this.baseUrl+'/admin/create-menu',newMenu,this.httpOptions);
}

//find-menu/:id'  update-menu/:id

getMenuItem(menuId:any){
  return this.http.get<{message:string, data:MenuOutput}>(this.baseUrl+'/admin/find-menu/'+menuId.toString());
}


updateMenu(id:any, menu:MenuInput){
  const newMenu = new FormData();
  newMenu.append("name", menu.name);
  newMenu.append("price", menu.price);
  newMenu.append("category", menu.category);
  newMenu.append("description", menu.description);
  newMenu.append("image", menu.image, menu.image? menu.image.name : '');
  return this.http.put<{message:string,data:MenuOutput}>(this.baseUrl+'/admin/update-menu/'+id.toString(),newMenu,this.httpOptions);
}

deleteMenu(id:any){
  return this.http.delete<{message:string,data:MenuOutput}>(this.baseUrl+'/admin/delete-menu/'+id.toString());
}

}
