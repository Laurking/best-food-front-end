import { Injectable } from '@angular/core';
import { Variables } from '../shared-features/variables/constant';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private baseUrl = Variables.getBaseUrl();
  private httpOptions = { headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
constructor(private http:HttpClient) {}

sendMessage(contact:Contact){
  return this.http.post(this.baseUrl+'/contact/send-message',JSON.stringify(contact),this.httpOptions);
}



}
