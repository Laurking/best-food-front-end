import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Variables } from '../shared-features/variables/constant';

import { MenuOutput, MenuInput } from '../models/menu.model';



@Injectable({
    providedIn: 'root'
})
export class ClientMenuService {
    private baseUrl = Variables.getBaseUrl();
    private referenceMenu: MenuOutput[];
    
    constructor(private http:HttpClient, private router:Router) {}
    
    getMenuList(){
        return this.http.get<{message:string, data: MenuOutput[]}>(this.baseUrl+'/retrieve-all-menu');
    }

    getMenuItem(id:any){
        return this.http.get<{message:string, data: MenuOutput}>(this.baseUrl+'/find-menu/'+id.toString()); 
    }
    
    searchMenuItem(value:String){
        let criteria = {criteria:value};
        return this.http.post<{message:string, data: MenuOutput[]}>(this.baseUrl+'/search-menu/',criteria); 
    }

    findMenuByCategory(category:string){
        let categoryDetails = {category:category};
        return this.http.post<{message:string, data: MenuOutput[]}>(this.baseUrl+'/retrieve-category',categoryDetails); 
    }
}
