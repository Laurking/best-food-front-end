import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Variables } from '../shared-features/variables/constant';
import { User } from '../models/user.model';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  private baseUrl = Variables.getBaseUrl();
  private retrievedUsers:User[]=[];
  private updatedUsers = new Subject<{users:User[]}>();
  private httpOptions = { headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
  constructor(
    private router:Router,
    private http:HttpClient,
    private flashMessageService:FlashMessagesService
    ) {}

    createUser(userData){
      let newUser = JSON.stringify(userData);
      return this.http.post<{message:string}>(this.baseUrl+'/users/create-user',newUser, this.httpOptions);
    }

    retrieveAllUsers(){
      return this.http.get<{message:string,users:User[]}>(this.baseUrl+'/users/retrieve-inactive-users',this.httpOptions).subscribe(response =>{
        this.retrievedUsers = response.users;
        this.updatedUsers.next({users:[...this.retrievedUsers]});
      },error =>{
        console.log(error);
      });
    }

    activateUser(userId:any){
      return this.http.put<{message:string}>(this.baseUrl+'/users/activate-user/'+userId.toString(),this.httpOptions).subscribe(response =>{
        this.flashMessageService.show('User successfully activated', { cssClass: 'alert-success', timeout: 3000 });
        setTimeout(()=>{
          this.retrievedUsers.map((user) => user._id == userId ? user.active = true : null);
        this.updatedUsers.next({users:[...this.retrievedUsers]});
        },3000)
      },error =>{
        console.log(error);
      });
    }


    deactivateUser(userId:any){
      return this.http.put<{message:string}>(this.baseUrl+'/users/deactivate-user/'+userId.toString(),this.httpOptions).subscribe(response =>{
        this.flashMessageService.show('User successfully deactivated', { cssClass: 'alert-success', timeout: 3000 });
        setTimeout(()=>{
          this.retrievedUsers.map((user) => user._id == userId ? user.active = false : null);
        this.updatedUsers.next({users:[...this.retrievedUsers]});
        },3000);
      },error =>{
        console.log(error); 
      });
    }

    deleteUser(userId:any){
      return this.http.delete<{message:string}>(this.baseUrl+'/users/delete-user/'+userId.toString(),this.httpOptions).subscribe(response =>{
        this.retrievedUsers = this.retrievedUsers.filter(user => user._id != userId);
        this.updatedUsers.next({users:[...this.retrievedUsers]});
        this.flashMessageService.show('User successfully deleted', { cssClass: 'alert-success', timeout: 3000 });
      },error =>{
        console.log(error); 
      });
    }

    getUsers(){
      return this.updatedUsers.asObservable();
    }

    updateUserPassword(passwordUpdateDetails){
      let details = JSON.stringify(passwordUpdateDetails);
      return this.http.put<{message:string,passwordUpdatedAt:string}>(this.baseUrl+'/users/update-password',details, this.httpOptions);
    }
}
