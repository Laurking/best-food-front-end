import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Variables } from '../shared-features/variables/constant';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = Variables.getBaseUrl();
  private authStatusListener = new Subject<boolean>();
  private authenticationErrorListener = new Subject<HttpErrorResponse>();
  private isAuthenticated:boolean;
  private token:string;
  private tokenTimer: any;
  private user:User;
  private activeRoute:string;
  private userDataListener = new Subject<{user:User}>();

  private httpOptions = { headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

constructor(
  private router:Router,
  private http:HttpClient,
  private flashMessageService:FlashMessagesService
  ) {}
  
  getAuthStatusListener(){
    return this.authStatusListener.asObservable();
  }
  
  setAuthStatusListener(status:boolean){
    this.authStatusListener.next(status);
  }
  
  getAuthenticationErrorListener(){
    return this.authenticationErrorListener.asObservable();
  }
  
  getToken(){
    return this.token;
  }
  
  getIsAuthenticated(){
    return this.isAuthenticated;
  }
  
  getUser(){
    return this.user;
  }
 
  getUserDataListener(){
    return this.userDataListener.asObservable();
  }

  setUser(user:User){
    this.user = user;
    this.userDataListener.next({user: user});
    localStorage.setItem('user', JSON.stringify(user));
  }

  setActiveRoute(activeRoute:string){
    this.activeRoute = activeRoute;
  }

  navigateToActiveRoute(){
    if(this.activeRoute){
      this.router.navigate([this.activeRoute]);
    }
    else{
      this.router.navigate(['/africanamericanbestfood/admin/'])
    }
    this.flashMessageService.show('successfully signed in', { cssClass: 'alert-success', timeout: 3000 });
  }

  updateCurrentUserInfo(userData){
    let user = JSON.stringify(userData);
    return this.http.put<{user:User}>(this.baseUrl+'/users/update-user-personal-information',user,this.httpOptions);
  }

  

  signin(signinDetails){
    let credentials = JSON.stringify(signinDetails);
    this.http.post<{message:string, user:User, auth:{token: string,expiresIn:number}}>(this.baseUrl+'/users/signin',credentials,this.httpOptions).subscribe(response =>{
      this.token = response.auth.token;
      if (this.token) {
        this.user = response.user;
        this.userDataListener.next({ user: this.user});
        const expiresInDuration = response.auth.expiresIn;
        this.setAuthTimer(expiresInDuration);
        this.isAuthenticated = true;
        this.authStatusListener.next(true);
        const now = new Date();
        const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
        this.saveAuthData(this.token, expirationDate, this.user);
       this.navigateToActiveRoute();
      }
    },error =>{
      this.authenticationErrorListener.next(error);
    });
  }
  
  signup(signupDetails){
    let credentials = JSON.stringify(signupDetails);
    return this.http.post(this.baseUrl+'/users/signup',credentials, this.httpOptions);
  }
  
  logout(userAction:string) {
    this.token = null;
    this.isAuthenticated = false;
    this.user = null;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData(); this.router.navigate(['/africanamericanbestfood/admin/auth/signin']);
    if(userAction){
      this.flashMessageService.show('successfully logged out', { cssClass: 'alert-success', timeout: 3000 });
    }else{
      this.flashMessageService.show('Your session has expired. Please login', { cssClass: 'alert-danger', timeout: 5000 });
    }
  }
  
  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.user = authInformation.user;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
      //this.router.navigate(['/africanamericanbestfood/admin/'])
    }
  }
  
  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout(null);
    }, duration * 1000);
  }
  
  private saveAuthData(token: string, expirationDate: Date,user:User) {
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());
    localStorage.setItem('user', JSON.stringify(user));
  }
  
  private clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
    localStorage.removeItem("user");
  }
  
  
  private getAuthData() {
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const user = JSON.parse(localStorage.getItem('user'));
    if (!token || !expirationDate || !user) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      user:JSON.parse(localStorage.getItem('user'))
    }
  }

  forgotPassword(emailInput){
    let email = JSON.stringify(emailInput);
    return this.http.post(this.baseUrl+'/users/send-password-retrieve-token',email, this.httpOptions);
  }
  
  validateToken(token:string){
    let tokenObj = JSON.stringify({token: token});
    return this.http.post<{message:string,user:any}>(this.baseUrl+'/users/validate-token',tokenObj, this.httpOptions);
  }
  resetPassword(userData,id){
    let credentials = JSON.stringify(userData);
    return this.http.post(this.baseUrl+'/users/reset-new-password/'+id,credentials, this.httpOptions);
  }
  
}
