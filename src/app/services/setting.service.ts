import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Variables } from '../shared-features/variables/constant';
import { HomepageOutput, HomepageInput } from '../models/homepage.model';
import { AboutpageOutput, AboutpageInput } from '../models/aboutpage.model';


@Injectable({
  providedIn: 'root'
})
export class SettingService {
  private baseUrl = Variables.getBaseUrl();
  private httpOptions = { headers: new HttpHeaders({
    'Accept':'application/json'
  })
};
  constructor(
    private router:Router,
    private http:HttpClient,
    private flashMessageService:FlashMessagesService
    ) {}

    retrieveHomepageData(){
      return this.http.get<{message:string, data:HomepageOutput}>(this.baseUrl+'/client/retrieve-homepage-attributes');
    }

    retrieveAboutpageData(){
      return this.http.get<{message:string, data:AboutpageOutput}>(this.baseUrl+'/client/retrieve-aboutpage-attributes');
    }

    updateHomepageData(homepage: HomepageInput){
      const homepageData = new FormData();
      homepageData.append("header",homepage.header);
      homepageData.append("quote",homepage.quote);
      homepageData.append("image",homepage.image,homepage.image? homepage.image.name : '');
      return this.http.post<{message:string,data:HomepageOutput}>(this.baseUrl+'/client/update-homepage',homepageData,this.httpOptions);
      
    }

    updateAboutpageData(aboutpage: AboutpageInput){
      const aboutpageData = new FormData();
      aboutpageData.append("header", aboutpage.header);
      aboutpageData.append("quote", aboutpage.quote);
      aboutpageData.append("phone", aboutpage.phone);
      aboutpageData.append("topLeft", aboutpage.topLeft, aboutpage.topLeft? aboutpage.topLeft.name : '');
      aboutpageData.append("topRight", aboutpage.topRight, aboutpage.topRight? aboutpage.topRight.name : '');
      aboutpageData.append("bottomLeft", aboutpage.bottomLeft, aboutpage.bottomLeft? aboutpage.bottomLeft.name : '');
      aboutpageData.append("bottomCenter", aboutpage.bottomCenter, aboutpage.bottomCenter? aboutpage.bottomCenter.name : '');
      aboutpageData.append("bottomRight", aboutpage.bottomRight, aboutpage.bottomRight? aboutpage.bottomRight.name : '');
      return this.http.post<{message:string, data:AboutpageOutput}>(this.baseUrl+'/client/update-aboutpage/',aboutpageData,this.httpOptions);
    }
}
