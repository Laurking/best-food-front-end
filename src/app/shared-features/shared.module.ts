import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';



import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field'
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import { LayoutModule } from '@angular/cdk/layout';
import { 
  MatProgressSpinnerModule,MatCardModule, MatDatepickerModule,MatNativeDateModule,
  MatInputModule, MatMenuModule,MatListModule, MatIconModule,
  MatToolbarModule, MatButtonModule, MatSidenavModule,MatCheckboxModule,MatSelectModule
  } from '@angular/material';



import { Base64ConverterPipe } from 'src/app/pipes/base64converter.pipe';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SafeHtmlPipe } from '../pipes/safe-html.pipe';
import { NormalizerPipe } from '../pipes/normalizer.pipe';
import { PhoneNumberFormatterPipe } from '../pipes/phone-number.pipe';


@NgModule({
  declarations: [
    LoadingScreenComponent,
    SpinnerComponent,
    Base64ConverterPipe,
    SafeHtmlPipe,
    NormalizerPipe,
    PhoneNumberFormatterPipe
    
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatCardModule,
    LayoutModule,
    MatMenuModule,
    MatInputModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports:[
    MatMenuModule,
    FlexLayoutModule,
    MatCardModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    LoadingScreenComponent,
    SpinnerComponent,
    Base64ConverterPipe,
    SafeHtmlPipe,
    NormalizerPipe,
    PhoneNumberFormatterPipe
  ]
})
export class SharedModule { }
