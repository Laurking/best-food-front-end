import { FoodCategory } from 'src/app/models/food-category.model';

export class Variables{
    static getBaseUrl(){
        return "http://localhost:3000";
        // return "http://marialex.us-east-2.elasticbeanstalk.com";
    }
}

export class Food{
    private static categories:FoodCategory[]= [
        {name:'Main dish',displayName:'Main dish'},
        {name:'Side order',displayName:'Side order'},
        {name:'Drink',displayName:'Drink'},
        {name:'Dessert',displayName:'Dessert'}
        
    ];
    static getCategories(){
        return this.categories;
    }
}
