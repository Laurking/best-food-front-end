import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, CanActivateChild } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';



@Injectable()
export class AdminAuthenticationGuard implements CanActivate{
    constructor(private router:Router,private authServie:AuthService){}
    canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{
        const isAuthenticated = this.authServie.getIsAuthenticated();
        if(!isAuthenticated){
            // this.router.navigate(['africanamericanbestfood/admin/auth/signin']);
            this.router.navigate(['/africanamericanbestfood/admin/auth/signin'], {
                queryParams: {
                    redirectTo: state.url
                }
              });
        }
        return isAuthenticated;
    }
    
}


@Injectable()
export class AdminAuthorizationGuard implements CanActivate{
    constructor(private router:Router,private authServie:AuthService,
        private flashMessageService: FlashMessagesService
        ){}
        canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{
            const user = this.authServie.getUser();
            const isAuthorized = (user.active && user.userRole == "Owner") || (user.active && user.userRole == "Super Admin");
            if(!isAuthorized){
                this.router.navigate(['/africanamericanbestfood/admin/show-menu']);
                this.flashMessageService.show('You have no access or right to the resources', { cssClass: 'alert-danger', timeout: 3000 });
            }
            return isAuthorized;
        }
    }
    
    
    // @Injectable()
    // export class AdminUnauthorizedWhenAuthenticatedGuard implements CanActivateChild{
    //     private isAuthenticated:boolean = false;
    //     constructor(private router:Router,private authServie:AuthService,
    //         private flashMessageService: FlashMessagesService
    //         ){}
    //         canActivateChild(route:ActivatedRouteSnapshot, state:RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{
                
    //             this.authServie.getAuthStatusListener().subscribe(response =>{
    //             this.isAuthenticated = response;
    //             console.log(this.isAuthenticated+"   --------");
    //            },error =>{
    //                console.log(error);
    //            });
    //           if(this.isAuthenticated){
    //             this.router.navigate(['/'])
    //           }
    //           return this.isAuthenticated;
    //             // return this.authServie.getAuthStatusListener().subscribe(isAuth => {
    //             //     console.log('is authenticated',isAuth);
    //             //          if (isAuth) {
    //             //              return true;
    //             //          }else{
    //             //              this.router.navigate(['/signin']);
    //             //              return false;
    //             //          }
    //             //      });
    //         }
        // }