export interface FoodCategory{
    name:string;
    displayName:string;
}