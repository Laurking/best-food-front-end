export interface User{
    _id:string;
    firstname: string;
    lastname: string;
    birthDate:string;
    email:string;
    active: boolean;
    resetPasswordToken: ResetPasswordToken;
    userRole: string;
    passwordUpdatedAt:string;
}


interface ResetPasswordToken{
    value:string;
    expiresAt:Date;
}