export class ServerError{
    location:string;
    msg:string;
    param:string;
    value:string;
}

export class UniqueError{
    errorArray = []
    public set(errors:ServerError[]){
      this.uniqueSet(errors);
      return this.errorArray;
    }
    private uniqueSet(errors: ServerError[]){
      for(let error of errors){
        this.addUnique(error);
      }
    }
    private addUnique(error:ServerError){
      if(this.errorArray.length == 0){
        this.errorArray.push(error);
      }
      if(!this.contains(error)){
        this.errorArray.push(error);
      }
    }
    private contains(error){
      for(let err of this.errorArray){
        if(err.param == error.param){
          return true;
        }
      }
      return false;
    } 
  }