export interface HomepageInput{
    header:string;
    quote:string;
    image:File;
}

export interface HomepageOutput{
    header:string;
    quote:string;
    image:string;
}