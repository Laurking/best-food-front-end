export interface MenuInput{
    name:string;
    price:string;
    category:string,
    description:string;
    image:File;
}

export interface MenuOutput{
    name:string;
    price:string;
    category:string,
    description:string;
    image:string;
}