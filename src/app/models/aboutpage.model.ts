export interface AboutpageInput{
    header:string;
    quote:string;
    phone:string;
    topLeft:File;
    topRight:File;
    bottomLeft:File;
    bottomCenter:File;
    bottomRight:File;
}

export interface AboutpageOutput{
    header:string;
    quote:string;
    phone:string;
    topLeft:string;
    topRight:string;
    bottomLeft:string;
    bottomCenter:string;
    bottomRight:string;
}