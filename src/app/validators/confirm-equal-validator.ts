import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
    selector:'[confirmPasswordsMatchValidator]',
    providers:[{
        provide: NG_VALIDATORS,
        useExisting:ConfirmEqualValidatorDirective,
        multi:true
    }]
})
export class ConfirmEqualValidatorDirective implements Validator{

    @Input() confirmPasswordsMatchValidator:string
    validate(control: AbstractControl): {[key:string]: any} | null{
        const controlToCompare = control.parent.get(this.confirmPasswordsMatchValidator);
        if(controlToCompare && controlToCompare.value !== control.value){
            return { 'mismatch': true};
        }
        return null;
    }
}