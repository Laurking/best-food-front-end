import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateFileName'
})
export class NormalizerPipe implements PipeTransform {
  
  transform(filename: string, legnth: number): any {
    if(filename){
      const mimeType = filename.split('.')[1];
      if(filename.length > 20){
        return  filename.substring(0,19)+'...'+mimeType;
      }
      else{
        return filename;
      }
    }
    return null;
  }
  
}
