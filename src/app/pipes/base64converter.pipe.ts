import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'base64Converter'
})
export class Base64ConverterPipe implements PipeTransform {
  
  constructor(private sanitizer: DomSanitizer) {}
  transform(source: any, type:string): any {
    let url = '';
    if(type == null || type.length==0 || type == ''){
      url = 'data:image/png;base64,' + source;
    }
    else{
      url = 'data:image/'+type+';base64,' +  source;
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}



