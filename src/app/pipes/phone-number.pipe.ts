import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formatPhoneNumber'
})
export class PhoneNumberFormatterPipe implements PipeTransform {
    
    transform(number:string): string {
        var reg = new RegExp(/^\d+$/);
        var extracted = reg.exec(number).input;
        var formatted = "("+extracted.substring(0,3)+")"+extracted.substring(3,6)+" - "+extracted.substring(6,10);
        return formatted;
    }
    
}
