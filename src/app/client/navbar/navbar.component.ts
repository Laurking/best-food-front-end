import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuOutput } from 'src/app/models/menu.model';
import { ClientMenuService } from 'src/app/services/client-menu.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy{
  
  public results:MenuOutput[] = []
  public criteria:String;
  public searchSub:Subscription;
  constructor(public menuService:ClientMenuService) { }
  
  ngOnInit() {
  }
  
  search(criteria:String){
    if(criteria){
      this.searchSub = this.menuService.searchMenuItem(criteria).subscribe(response =>{
        this.results = response.data;
      })
      return;
    }
    else{
      this.results = [];
    }
    
  }
  ngOnDestroy(){
    this.searchSub.unsubscribe();
  }
}
