import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';




import { SharedModule } from '../shared-features/shared.module';



import { ClientRoutingModule } from './client-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ClientComponent } from './client.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { ClientDetailsComponent } from './details/details.component';
import { ClientMenuService } from '../services/client-menu.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';









@NgModule({
  declarations: [
    ClientComponent,
    NavbarComponent, 
    HomeComponent, 
    MenuListComponent, 
    ContactComponent, 
    AboutComponent, 
    FooterComponent,
    ClientDetailsComponent,
    PageNotFoundComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ClientRoutingModule    
  ],
  providers:[ClientMenuService]
})
export class ClientModule { }
