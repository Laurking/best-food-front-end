import { Component, OnInit, OnDestroy } from '@angular/core';
import { AboutpageOutput } from 'src/app/models/aboutpage.model';
import { Subscription } from 'rxjs';
import { SettingService } from 'src/app/services/setting.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {

  aboutPageData:AboutpageOutput;
  retrieveAboutPageSub:Subscription;
  dataRetrieved:boolean = false;
  constructor(public settingService:SettingService) { }
  
  ngOnInit() {
    this.retrieveAboutPageSub = this.settingService.retrieveAboutpageData().subscribe(response =>{
      this.aboutPageData = response.data;
      console.log(this.aboutPageData);
      this.dataRetrieved = true;
    },error =>{
      console.log(error);
      this.dataRetrieved = false;
    })
  }

  ngOnDestroy(){
    if(this.retrieveAboutPageSub){
      this.retrieveAboutPageSub.unsubscribe();
    }
  }
}
