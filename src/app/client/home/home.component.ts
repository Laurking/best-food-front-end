import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingService } from 'src/app/services/setting.service';
import { HomepageOutput } from 'src/app/models/homepage.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  homePageData:HomepageOutput;
  retrieveHomePageSub:Subscription;
  dataRetrieved:boolean = false;
  constructor(public settingService:SettingService) { }
  
  ngOnInit() {
    this.retrieveHomePageSub = this.settingService.retrieveHomepageData().subscribe(response =>{
      this.homePageData = response.data;
      this.dataRetrieved = true;
    },error =>{
      console.log(error);
      this.dataRetrieved = false;
    })
  }

  ngOnDestroy(){
    if(this.retrieveHomePageSub){
      this.retrieveHomePageSub.unsubscribe();
    }
  }
  
}
