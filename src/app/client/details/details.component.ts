import { Component, OnInit } from '@angular/core';
import { MenuOutput } from 'src/app/models/menu.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { ClientMenuService } from 'src/app/services/client-menu.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class ClientDetailsComponent implements OnInit {
  public menu:MenuOutput;
  public isLoading:boolean = true;
  public menuItemSub:Subscription;
  constructor(public menuService:ClientMenuService, public router:ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.menuItemSub = this.menuService.getMenuItem(params.id).subscribe(response =>{
        this.menu = response.data;
        this.isLoading = false;
      })
    });
    
  }

  ngOnDestroy(){
    this.menuItemSub.unsubscribe();
  }

}
