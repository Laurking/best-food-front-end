import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';
import { Router } from '@angular/router';
import { ServerError, UniqueError } from 'src/app/models/error.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
@ViewChild('confirmation') confirmation:ElementRef;
  public contactForm: FormGroup;
  public sendingMessageStatus:boolean = false;
  serverErrors:ServerError[] =[];
  errorMessage:any = null;
  constructor(
    public router:Router,
    public renderer:Renderer2,
    public contactService:ContactService) { }

  ngOnInit() {
    this.contactForm = this.createContactForm();
  }

  public createContactForm(){
    return new FormGroup({
      fullname: new FormControl("", [Validators.required, 
        Validators.pattern(/^[A-Za-z]{2,30}[\s|,][A-Za-z]{2,30}$/i)]),
      email: new FormControl("", [Validators.required, 
        Validators.pattern(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i)]),
      subject: new FormControl("", [Validators.required, 
        Validators.minLength(2), Validators.maxLength(20)]),
      message: new FormControl("", [Validators.required, 
        Validators.minLength(50), Validators.maxLength(500)]),
    });
  }

  onSubmit(){
    if(this.contactForm.invalid){
      return;
    }
    const contact:Contact={
      fullname: this.contactForm.value.fullname,
      email: this.contactForm.value.email,
      subject: this.contactForm.value.subject,
      message: this.contactForm.value.message
    }
    this.sendingMessageStatus = true;
    this.contactService.sendMessage(contact).subscribe(response =>{
      console.log(response);
      this.sendingMessageStatus = false;
      this.renderer.setStyle(this.confirmation.nativeElement,'display','block');
    }, error =>{
      this.sendingMessageStatus = false;
      this.handleError(error);
    })
  }


  get fullname(){
    return this.contactForm.get('fullname');
  }

  get email(){
    return this.contactForm.get('email');
  }

  get subject(){
    return this.contactForm.get('subject');
  }

  get message(){
    return this.contactForm.get('message');
  }

  

  handleError(error){
    if(error.status == 422){
      this.serverErrors = new UniqueError().set(error.error.errorData);
      console.log(this.serverErrors);
      return;
    }
    if(error.status == 500){
     this.errorMessage = "Server Error. Please try again later";
    }
    else{
      this.serverErrors=[];
      this.errorMessage=null;
    }
  }
}
