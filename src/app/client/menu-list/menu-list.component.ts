import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuOutput } from 'src/app/models/menu.model';
import { ClientMenuService } from 'src/app/services/client-menu.service';
import { Subscription } from 'rxjs';
import { FoodCategory } from 'src/app/models/food-category.model';
import { Food } from 'src/app/shared-features/variables/constant';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit, OnDestroy{
 public foodList:MenuOutput[] = []
 public foodListSub:Subscription;
 public menuCategorySub:Subscription;
 public isLoading:boolean = true;
 public categories:FoodCategory[];
  constructor(public menuService:ClientMenuService) { 
  }

  ngOnInit() {
    this.categories = Food.getCategories();
    this.foodListSub = this.menuService.getMenuList().subscribe(response=>{
      this.foodList = response.data;
      this.isLoading = false;
    })
  }

  findCategory(category){
    this.menuCategorySub = this.menuService.findMenuByCategory(category).subscribe(response =>{
      this.foodList = response.data;
    },error=>{
      console.log(error);
    })
  }

  ngOnDestroy(){
    if(this.foodListSub){
      this.foodListSub.unsubscribe();
    }
    if(this.menuCategorySub){
      this.menuCategorySub.unsubscribe();
    }
  }
  
}
