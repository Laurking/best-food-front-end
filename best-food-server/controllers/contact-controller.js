const Contact = require('../models/contact');
const Mailer = require('../mailer/mailer-client');
const constants = require('../constants/variables');
const NoteTemplate = require('../mailer/templates');
const Normalizer = require('../utils/data-field-normalizer');

const { validationResult } = require('express-validator/check');

exports.sendMessage = async(request, response, next)=>{
   const fullname = request.body.fullname;
   const email = request.body.email;
   const subjectvalue = request.body.subject;
   const messageValue = request.body.message;
   const errorArrays = validationResult(request);
   if(!errorArrays.isEmpty()){
      return response.status(422).json({
         message: 'Please provide valid data',
         errorData: errorArrays.array()
      });  
   }
   try{
      const contact = new Contact({
         fullname: new Normalizer(fullname).normalizeString().capitalizeFullName().getStringValue(),
         email: new Normalizer(email).normalizeString().toLoweCase().getStringValue(),
         subject: new Normalizer(subjectvalue).normalizeString().capitalize().getStringValue(),
         message: new Normalizer(messageValue).normalizeString().getStringValue(),
      });
      const savedContact = await contact.save()
      const subject = 'Thank you for contacting us';
      const message = NoteTemplate.ThankYouNoteFromContact;
      const mailerResponse = await Mailer.sendMail(constants.customerService, savedContact.email, subject, message);
      return response.status(200).json({
         message: 'message successfully sent',
         data: mailerResponse
      });
   }catch(err){
      return response.status(500).json({message:'failed to send message', error: error});
   }
}

exports.getAllMessages = async(request, response, next)=>{
   try{
      const constacts = await Contact.find();
      return response.status(200).json({message:'contacts successfully retrieved', data: constacts})
  }catch(error){
      return response.status(500).json({message:'failed to retrieve contacts', error: error});
  }
}


exports.deleteOneMessage = async(request, response, next)=>{
   try{
      const constact = await Contact.findOneAndRemove({_id:request.params.id});
      return response.status(200).json({message:'contact successfully deleted', data: constact});
  }catch(error){
      return response.status(500).json({message:'failed to deleted contacts', error: error});
  }
}


exports.deleteAllMessages = async(request, response, next)=>{
   try{
      const constacts = await Contact.remove();
      return response.status(200).json({message:'contacts successfully deleted', data: constacts});
  }catch(error){
      return response.status(500).json({message:'failed to deleted contacts', error: error});
  }
}



