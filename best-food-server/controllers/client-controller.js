const Menu = require('../models/menu');

exports.getOneMenu = async(request, response, next)=>{
    try{
        const menu =  await Menu.findOne({_id:request.params.id});
        return response.status(200).json({message:'menu successfully retrieved', data: menu})
    }catch(error){
        return response.status(500).json({message:'failed to retrieve menu',error:error})
    }
}

exports.getAllMenu = async(request, response, next)=>{
    try{
        const menu =  await Menu.find();
        return response.status(200).json({message:'menus successfully retrieved', data: menu})
    }catch(error){
        return response.status(500).json({message:'failed to retrieve menus',error:error})
    }
}


exports.searchMenu = async(request, response, next) =>{
    try{
        const searchElement = request.body.criteria;
        if(!searchElement){
            return response.status(200).json({message:'no data found', data: []});
        }
        const menu = await Menu.find(
            {name: { $regex: '.*' + request.body.criteria + '.*', $options: 'i' } });
            
            return response.status(200).json({message:'menu successfully found', data: menu});
        }catch(error){
            return response.status(500).json({message:'failed to find menu', error: error});
        }
    }

    exports.findMenuByCategory = async(request, response, next) =>{
        try{
            const menu = await Menu.find(
                {category: { $regex: '.*' + request.body.category + '.*', $options: 'i' } });                
                return response.status(200).json({message:'menu successfully found', data: menu});
            }catch(error){
                return response.status(500).json({message:'failed to find menu', error: error});
            }
        }
    
    // exports.searchMenu = async(request, response, next) =>{
    //     try{
    //         const searchElement = request.body.criteria;
    //         if(!searchElement){
    //             return response.status(200).json({message:'no data found', data: []});
    //         }
    //        const menu = await Menu.find(
    //            { $or:[
    //            {name: { $regex: '.*' + request.body.criteria + '.*', $options: 'i' } },
    //            {description: { $regex: '.*' + request.body.criteria + '.*', $options: 'i' } },
    //        ]});
    //        return response.status(200).json({message:'menu successfully found', data: menu});
    //    }catch(error){
    //        return response.status(500).json({message:'failed to find menu', error: error});
    //    }
    //  }

    