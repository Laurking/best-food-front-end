const fileSystem = require('fs');
const Homepage = require('../models/client-homepage');
const Normalizer = require('../utils/data-field-normalizer');
const { validationResult } = require('express-validator/check');

exports.updateHomepage = async(request, response, next)=>{
    const errorArray = validationResult(request);
    if(!errorArray.isEmpty()){
        console.log(errorArray.array());
        return response.status(422).json({message:'Please provide valid data', errorData: errorArray.array()})
    }
    try{
        const homepage = await Homepage.findOne({});
        if(!homepage){
            if(!request.file){
                return response.status(422).json({message:'Please provide valid image'})
            }
            const img = fileSystem.readFileSync(request.file.path);
            const encoded_image = img.toString('base64');
            const  homepageData = new Homepage({
                header: request.body.header,
                quote: request.body.quote,
                image: encoded_image,
            });
            const savedHomepageData  = await homepageData.save();
            return response.status(200).json({message:'homepage data successfully updated', data: savedHomepageData});
        }
        if(request.file){
            const img = fileSystem.readFileSync(request.file.path);
            const encoded_image = img.toString('base64');
            homepage.image = encoded_image;
        }
        homepage.header = request.body.header;
        homepage.quote = request.body.quote;
        const savedHomepageData = await homepage.save();
        return response.status(200).json({message:'homepage data successfully updated', data: savedHomepageData});
    }catch(error){
        return response.status(500).json({message:'failed to update homepage data', error: error})
    }
    
}


exports.retrieveHomepageAttributes = async(request, response, next) =>{
    try{
        const homepageData = await Homepage.findOne({});
        return response.status(200).json({message:'home page attributes successfully retrieved', data: homepageData});
    }catch(err){
        return response.status(500).json({message:'failed to retrieve home page attributes',error:err});
    }
}