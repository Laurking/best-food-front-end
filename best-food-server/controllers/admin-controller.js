const fileSystem = require('fs');
const Menu = require('../models/menu');
const Normalizer = require('../utils/data-field-normalizer');
const { validationResult } = require('express-validator/check');

exports.createMenu = async(request, response,nex) =>{ 
    const errorArray = validationResult(request);
    if(!errorArray.isEmpty()){
        return response.status(422).json({message:'Please provide valid data',errorData: errorArray.array()})
    }
    try{
        const img = fileSystem.readFileSync(request.file.path);
        const encoded_image = img.toString('base64');
        const menu = new Menu({
            name: new Normalizer(request.body.name).normalizeString().capitalize().getStringValue(), 
            price: request.body.price,
            category:request.body.category,
            description: new Normalizer(request.body.description).normalizeString().capitalize().getStringValue(), 
            image: encoded_image,
            createdBy:request.user._id,
            updatedBy:request.user._id,
        });
        const newMenu = await menu.save()
        response.status(200)
        .json({message:'menu successfully created', data: newMenu});
    }catch(error){
        return response.status(500)
        .json({message: 'failed to create menu'})
    }
};



exports.retrieveAllMenu = async (request, response, next)=>{
    try{
        const menu = await Menu.find();
        return response.status(200).json({message:'menus successfully retrieved', data: menu})
    }catch(error){
        return response.status(500).json({message:'failed to retrieve menus', error: error})
    }
}


exports.retrieveMenu = async (request, response, next)=>{
    try{
        const menu = await Menu.findOne({_id:request.params.id});
        return response.status(200).json({message:'menu successfully retrieved', data: menu})
    }catch(error){
        return response.status(500).json({message:'failed to retrieve menu', error: error})
    }
}

exports.updateMenu = async(request, response, next)=>{
    const errorArray = validationResult(request);
    if(!errorArray.isEmpty()){
        return response.status(422).json({message:'Please provide valid data', errorData: errorArray.array()})
    }
    try{
        const menu = await Menu.findOne({_id:request.params.id});
        if(request.file){
            const img = fileSystem.readFileSync(request.file.path);
            const encoded_image = img.toString('base64');
            menu.image = encoded_image;
        }
        menu.name = new Normalizer(request.body.name).normalizeString().capitalize().getStringValue(), 
        menu.price = request.body.price,
        menu.category = request.body.category,
        menu.description = new Normalizer(request.body.description).normalizeString().capitalize().getStringValue(), 
        menu.updatedBy = request.user._id,
        menu.updated_at = Date.now();
        const updatedMenu = await menu.save();
        return response.status(200).json({message:'menu successfully update', data: updatedMenu})
    }catch(error){
        return response.status(500).json({message:'failed to update menu', error: error})
    }
}


exports.deleteOneMenu = async (request, response, next)=>{
    try{
        const deletedMenu = await Menu.findOneAndRemove({_id:request.params.id});
        return response.status(200).json({message:'menu successfully deleted', data: deletedMenu})
    }catch(error){
        return response.status(500).json({message:'failed to delete menu', error: error})
    }
}
