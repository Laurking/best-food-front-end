const User = require('../models/user');
const Bcrypt = require('bcryptjs');
const constants = require('../constants/variables');
const Mailer = require('../mailer/mailer-client');
const NoteTemplate = require('../mailer/templates');
const TokenGenerator = require('../utils/token-generator');
const RandomPasswordGenerator = require('../utils/random-password-generator');
const Normalizer = require('../utils/data-field-normalizer');
const { validationResult } = require('express-validator/check');
const jwt = require('jsonwebtoken');

exports.signup = async(request, response, next)=>{
    const email = request.body.email;
    const password = request.body.password;
    try{
        const numberOfUsers = await User.countDocuments({});
        if(numberOfUsers >= 2){
            return response.status(401).json({message:'Your request is unauthorized.'});
        }
    }catch(err){
        return response.status(500).json({message:'failed to sign up',error:err});
    }
    
    const errorArrays = validationResult(request);
    if(!errorArrays.isEmpty()){
        return response.status(422).json({
            message:'Please provide valid data',
            errorData: errorArrays.array()
        });
    }
    try{
        const hashedPassword = await Bcrypt.hash(password, 12)
        const user =  new User({
            email: new Normalizer(email).normalizeString().toLoweCase().getStringValue(), 
            password:hashedPassword,
        });
        await user.save();
        return response.status(200).json({ message:'successfully signed up'});
    }catch(err){
        return response.status(500).json({message:'failed to sign up',error:err});
    }
}

exports.signin = async (request, response, next)=>{
    const email = request.body.email;
    const password = request.body.password;
    const errorArrays = validationResult(request);
    if(!errorArrays.isEmpty()){
        return response.status(422).json({
            message:'invalid email or password',
            errorData: errorArrays.array(),
        });
    }
    try{
        const user = await User.findOne({
            email: new Normalizer(email).normalizeString().toLoweCase().getStringValue()
        });
        if(user){ 
            const doMatch = await Bcrypt.compare(password, user.password);
            if (doMatch) {  
                const userResponse = {
                    _id:user._id,
                    firstname:user.firstname, 
                    lastname:user.lastname,
                    email: user.email,
                    birthDate:user.birthDate,
                    active:user.active,
                    userRole:user.userRole,
                    resetPasswordToken:user.resetPasswordToken,
                    passwordUpdatedAt:user.passwordUpdated_at
                }
                const token = jwt.sign({user:userResponse},
                    constants.jwtSecret,{expiresIn:"1h"});
                    
                    return response.status(200).json({message:'successfully logged in', user:userResponse,  auth:{token: token,expiresIn:3600} });
                }
                
                return response.status(422).json({message:'invalid email or password'});
            }
            return response.status(422).json({message:'invalid email or password'});
        } catch(err) {
            return response.status(500).json({message:'failed to sign in',error:err});
        }
    }
    
    exports.updateUser = async(request, response, next)=>{
        const userId = request.body.userId;
        const firstname = request.body.firstname;
        const lastname = request.body.lastname
        const birthDate = request.body.birthDate
        const errorArrays = validationResult(request);
        if(!errorArrays.isEmpty()){
            return response.status(422).json({
                message:'Please provide valid data',
                errorData: errorArrays.array(),
            });
        }
        try{
            const user = await User.findOne({_id:userId});
            user.firstname = firstname;
            user.lastname = lastname;
            user.birthDate = birthDate;
            user.updated_at = Date.now();
            await user.save();
            const updatedUser = await User.findOne({_id:userId}).select("-password -created_at -updated_at");
            const userResponse = {
                _id:updatedUser._id,
                firstname:updatedUser.firstname, 
                lastname:updatedUser.lastname,
                email: updatedUser.email,
                birthDate:updatedUser.birthDate,
                active:updatedUser.active,
                userRole:updatedUser.userRole,
                resetPasswordToken:updatedUser.resetPasswordToken,
                passwordUpdatedAt:updatedUser.passwordUpdated_at
            }
            
            return response.status(200).json({message:'user successfully updated',user:userResponse});
        }catch(err){
            return response.status(500).json({message:'failed to update user',error:err});
        }
        
    }
    exports.createUser = async(request, response, next)=>{
        const errorArrays = validationResult(request);
        if(!errorArrays.isEmpty()){
            return response.status(422).json({
                message:'Please provide valid data',
                errorData: errorArrays.array()
            });
            
        }
        try{
            const password = RandomPasswordGenerator.getRandomPassword();
            const hashedPassword = await Bcrypt.hash(password, 12)
            const user =  new User({
                email: new Normalizer(request.body.email).normalizeString().toLoweCase().getStringValue(), 
                password:hashedPassword,
                userRole:request.body.role,
                passwordExpirationTime: Date.now()+ constants.passwordvAlidationDeadline,
                active:false
            });
            await user.save();
            const subject = 'African American Best Food team membership';
            const message = NoteTemplate.passwordDetails(constants.loginUri,password)
            const mailerResponse = await Mailer.sendMail(constants.noReply, user.email, subject, message);
            return response.status(200).json({ message:'successfully created user',
            data: mailerResponse});
        }catch(err){
            return response.status(500).json({message:'failed to created user',error:err});
        }
    }
    
    exports.retrieveAllUsers=async(request, response, next)=>{
        try{
            const users = await User.find().select("-password -created_at -updated_at");
            return response.status(200).json({message:'users successfully retrieved',users: users});
        }catch(err){
            return response.status(500).json({message:'failed to retrieve users',error:err});
        }
    }
    
    exports.activateUser = async(request, response, next)=>{
        try{
            const user = await User.findOne({_id:request.params.id});
            user.active = true;
            user.updated_at = Date.now();
            await user.save()
            return response.status(200).json({message:'user successfully activated'});
        }catch(err){
            return response.status(500).json({message:'failed to activate user',error:err});
        }
    }
    
    exports.deactivateUser = async(request, response, next)=>{
        try{
            const user = await User.findOne({_id:request.params.id});
            user.active = false;
            user.updated_at = Date.now();
            await user.save()
            return response.status(200).json({message:'user successfully deactivated'});
        }catch(err){
            return response.status(500).json({message:'failed to deactivate user',error:err});
        }
    }
    
    exports.deleteUser = async(request, response, next)=>{
        try{
            await User.findOneAndRemove({_id:request.params.id});
            return response.status(200).json({message:'user successfully deleted'});
        }catch(err){
            return response.status(500).json({message:'failed to retrieve users',error:err});
        }
    }
    
    
    
    // exports.postForgotPassword = async(request, response, next)=>{
    //     const email = request.body.email;
    //     const errorArrays = validationResult(request);
    //     if(!errorArrays.isEmpty()){
    //         return response.render('shop/pages/forgot-password',{
    //             currentPage: 'forgot password', title:'Forgot password',
    //             oldInputValue:{email: email}, errorMessages: errorArrays.array()
    //         });
    //     }
    //     try{
    //         let user = await User.findOne({email: new Normalizer(email).normalizeString().toLoweCase().getStringValue()})
    //         user.token.value = TokenGenerator.generateToken();
    //         user.token.expiresAt = Date.now()+3600000;
    //         user.save();
    //         const subject = 'Please follow the below instructions to change your password';
    //         const message = NoteTemplate.resetPassword(user.token.value);
    //         Mailer.sendMail(constants.customerService, user.email, subject, message);
    //         return response.render('shop/pages/password-change-token-sent',{
    //             currentPage:'password-change', title:'Password change information'
    //         });
    //     }catch(err){
    //         const error = new Error(err);
    //         error.httpStatusCode = 500;
    //         return next(error);
    //     }
    // }
    
    // exports.validatePasswordResetToken = async(request, response, next)=>{
    //     const token = request.params.token;
    //     try{
    //         const user = await User.findOne({'token.value':token, 'token.expiresAt':{$gt: new Date()}})
    //         if(!user){
    //             return response.render('shop/pages/forgot-password',{
    //                 currentPage: 'forgot password', title:'Forgot password',
    //                 errorMessages:[{ location: 'body',param: 'email',msg: 'This request expired. Please try again' }],
    //                 oldInputValue:{email: ''}
    //             });
    //         }
    //         return response.render('shop/pages/new-password', {
    //             currentPage: 'new password', title:'New password',
    //             oldInputValue:{ password: '', confirmPassword: ''},
    //             errorMessages:[],
    //             userId: user._id
    //         })  
    //     }catch(err){
    //         const error = new Error(err);
    //         error.httpStatusCode = 500;
    //         return next(error);
    //     }
    // }
    
    exports.postNewPassword = async (request, response, next)=>{ // when user deliberately want to change their password
        const currentPassword = request.body.currentPassword;
        const newPassword = request.body.newPassword;
        const userId = request.body.userId;
        const errorArrays = validationResult(request);
        if(!errorArrays.isEmpty()){
            return response.status(422).json({message:'Please enter valid data', errorData:errorArrays.array()})
        }
        try{
            const user = await User.findOne({_id:userId});
            const passwordMatch = await Bcrypt.compare(currentPassword,user.password);
            if(passwordMatch){
                const passwordUpdatedTime = new Date();
                const hashedPassword = await Bcrypt.hash(newPassword,12);
                user.password = hashedPassword;
                user.passwordUpdated_at = passwordUpdatedTime;
                await user.save();
                return response.status(200).json({
                    message:'Password successfully updated',
                    passwordUpdatedAt:passwordUpdatedTime
                });
            }
            return response.status(401).json({message:'Current password is wrong'});
        }catch(err ){
            console.log(err);
            return response.status(500).json({message:'failed to update password',error:err});
        }
    }
    
    
    exports.sentPasswordRetrieveTokenToUser = async(request, response, next)=>{
        try{
            const errorArrays = validationResult(request);
            if(!errorArrays.isEmpty()){
                return response.status(422).json({message:'Please enter valid data', errorData:errorArrays.array()})
            }
            const user = await User.findOne({email:request.body.email});
            const tokenExpiration = new Date().getTime() + 3600000
            const token = TokenGenerator.generateToken();
            user.resetPasswordToken.value = token;
            user.resetPasswordToken.expiresAt = tokenExpiration;
            await user.save();
            const subject = 'Best Food Password Reset Request Action Required';
            const message = NoteTemplate.resetPassword(token);
            const mailerResponse = await Mailer.sendMail(constants.noReply, user.email, subject, message);
            return response.status(200).json({
                message: 'message successfully sent',
                data: mailerResponse
            });
        }catch(error ){
            return response.status(500).json({message:'failed to send password retrival token', error: error}); 
        }
    }
    
    exports.validateToken = async (request, response, next)=>{
        const token = request.body.token;
        try{
            const user = await User.findOne({'resetPasswordToken.value':token})
            .select('_id resetPasswordToken.expiresAt');
            return response.status(200).json({message:'token', user:user});
        }catch(error){
            return response.status(500).json({message:'invalid token', error:error});
        }
    }
    
    exports.resetNewPassword = async (request, response, next)=>{
        const userId = request.params.id;
        const newPassword = request.body.password;
        try{
            const user = await User.findOne({_id:userId});
            if(!user){
                return response.status(201).json({message:'error retrieving users'}); 
            }
            const hashedPassword = await Bcrypt.hash(newPassword,12);
            user.password = hashedPassword;
            user.resetPasswordToken.value = null;
            user.resetPasswordToken.expiresAt = null;
            user.passwordUpdated_at = new Date();
            await user.save();
            return response.status(200).json({message:'successfully reset password'})
        }catch(error){
            return response.status(500).json({message:'failed to reset password', error: error}); 
        }
    }
    
    
    
    