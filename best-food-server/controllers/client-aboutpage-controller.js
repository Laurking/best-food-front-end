const fileSystem = require('fs');
const Aboutpage = require('../models/client-aboutpage');
const Normalizer = require('../utils/data-field-normalizer');
const { validationResult } = require('express-validator/check');


exports.updateAboutpage = async (request, response, next) =>{
    const errorArray = validationResult(request);
    if(!errorArray.isEmpty()){
        return response.status(422).json({message:'Please provide valid data', errorData: errorArray.array()})
    }
    try{
        
        const aboutpage = await Aboutpage.findOne({});
        if(!aboutpage){
            if(!request.files.topLeft || !request.files.topRight || !request.files.bottomLeft
               || !request.files.bottomCenter || !request.files.bottomRight){
                return response.status(422).json({message:'aboutpage file data not received'});
            }
            const topLeft = fileSystem.readFileSync(request.files.topLeft[0].path);
            const topLeftTosBase64 = topLeft.toString('base64');
            const topRight = fileSystem.readFileSync(request.files.topRight[0].path);
            const topRightTosBase64 = topRight.toString('base64');
            const bottomLeft = fileSystem.readFileSync(request.files.bottomLeft[0].path);
            const bottomLeftTosBase64 = bottomLeft.toString('base64');
            const bottomCenter = fileSystem.readFileSync(request.files.bottomCenter[0].path);
            const bottomCenterTosBase64 = bottomCenter.toString('base64');
            const bottomRight = fileSystem.readFileSync(request.files.bottomRight[0].path);
            const bottomRightTosBase64 = bottomRight.toString('base64');
            const aboutpageData = new Aboutpage({
                header: request.body.header,
                quote: request.body.quote,
                phone: request.body.phone,
                topLeft: topLeftTosBase64,
                topRight: topRightTosBase64,
                bottomLeft: bottomLeftTosBase64,
                bottomCenter: bottomCenterTosBase64,
                bottomRight: bottomRightTosBase64
            })
            const savedAboutpageData  = await aboutpageData.save();
            return response.status(200).json({message:'aboutpage data successfully updated', data: savedAboutpageData});
        }
        if(request.files.topLeft){
            const topLeft = fileSystem.readFileSync(request.files.topLeft[0].path);
            const topLeftTosBase64 = topLeft.toString('base64');
            aboutpage.topLeft = topLeftTosBase64;
        }
        if(request.files.topRight){
            const topRight = fileSystem.readFileSync(request.files.topRight[0].path);
            const topRightTosBase64 = topRight.toString('base64');
            aboutpage.topRight = topRightTosBase64;
        }
        if(request.files.bottomLeft){
            const bottomLeft = fileSystem.readFileSync(request.files.bottomLeft[0].path);
            const bottomLeftTosBase64 = bottomLeft.toString('base64');
            aboutpage.bottomLeft = bottomLeftTosBase64;
        }
        if(request.files.bottomCenter){
            const bottomCenter = fileSystem.readFileSync(request.files.bottomCenter[0].path);
            const bottomCenterTosBase64 = bottomCenter.toString('base64');
            aboutpage.bottomCenter = bottomCenterTosBase64;
        }
        if(request.files.bottomRight){
            const bottomRight = fileSystem.readFileSync(request.files.bottomRight[0].path);
            const bottomRightTosBase64 = bottomRight.toString('base64');
            aboutpage.bottomRight = bottomRightTosBase64;
        }
        aboutpage.header = request.body.header;
        aboutpage.quote = request.body.quote;
        aboutpage.phone = request.body.phone;
        
        const savedAboutpageData  = await aboutpage.save();
        return response.status(200).json({message:'aboutpage data successfully updated', data: savedAboutpageData});
    }catch(error){
        console.log(error);
        return response.status(500).json({message:'failed to save about page data', error: error});
    }
}

exports.retrieveAboutpageAttributes = async(request, response, next)=>{
    try{
        const aboutpageData = await Aboutpage.findOne({});
        return response.status(200).json({message: 'aboutpage data successfully retrieved', data: aboutpageData});
    }catch(error){
        return response.status(500).json({message: 'failed to retrieve aboutpage data', error: error});
    }
}