const jwt = require('jsonwebtoken');
const constants = require('../constants/variables');

exports.canActivate = (request, response, next) =>{
    try{
        const token = request.headers.authorization.split(" ")[1];
        const decodedToken = jwt.verify(token, constants.jwtSecret);
        const user = decodedToken.user;
        const authorizedUser = (user.active && user.userRole == "Owner") || (user.active && user.userRole == "Super Admin");
        if(!authorizedUser){
            return response.status(401).json({message:'Unauthorized request'});
        }
        request.user = user;
        next();
    }catch(error){
        return response.status(401).json({message:'Authentication failed'});
    }
}