const express = require('express');
const router = express.Router();
const AuthValidator = require('../utils/auth-validator');
const StandardValidator = require('../utils/standard-validator');
const userController = require('../controllers/user-controller');
const adminAuthGuard = require('../authguards/admin-auth-guard');

router.post('/signup',[
    AuthValidator.validateSignupEmail(),
    AuthValidator.validateSignupPassword(),
    AuthValidator.validateConfirmPassword()
], userController.signup);

router.post('/signin', [
    AuthValidator.validateSigninEmail(),
    AuthValidator.validateSigninPassword()
], userController.signin);

router.put('/update-user-personal-information',
adminAuthGuard.canActivate,
[
StandardValidator.validateFirstName(),
StandardValidator.validateLastName(),
StandardValidator.validateBirthDate()
], userController.updateUser);

router.post('/create-user',
adminAuthGuard.canActivate,
[AuthValidator.validateSignupEmail(),
AuthValidator.validateUserRole()
],userController.createUser);


router.get('/retrieve-inactive-users',
adminAuthGuard.canActivate,
userController.retrieveAllUsers);

router.put('/activate-user/:id',
adminAuthGuard.canActivate,
userController.activateUser);

router.put('/deactivate-user/:id',
adminAuthGuard.canActivate,
userController.deactivateUser);


router.delete('/delete-user/:id', 
adminAuthGuard.canActivate,
userController.deleteUser);


// router.put('/forgot-pasword', userController.postForgotPassword);

// router.put('/validate-reset-token', userController.validatePasswordResetToken);

router.put('/update-password',
adminAuthGuard.canActivate,
AuthValidator.validateCurrentPassword(),
AuthValidator.validateNewPassword(),
AuthValidator.validateConfirmNewPassword(),
userController.postNewPassword);

router.post('/send-password-retrieve-token',
StandardValidator.validateEmail(),
userController.sentPasswordRetrieveTokenToUser);

router.post('/validate-token',
userController.validateToken)

router.post('/reset-new-password/:id',
[StandardValidator.validatePassword(),
StandardValidator.validateConfirmPassword()],
userController.resetNewPassword)

module.exports  = router;