const express = require('express');
const adminAuthGuard = require('../authguards/admin-auth-guard');
const fileExtractor = require('../utils/single-file-extractor');
const clientHomepageController = require('../controllers/client-homepage-controller');
const standardValidator = require('../utils/standard-validator');

const router = express.Router();;


router.post('/update-homepage',adminAuthGuard.canActivate,
 fileExtractor,[
    standardValidator.validateHomepageHeader(),
    standardValidator.validateHomepageQuote()
], 
clientHomepageController.updateHomepage);

router.get('/retrieve-homepage-attributes', 
clientHomepageController.retrieveHomepageAttributes);

module.exports  = router;