const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin-controller');
const adminAuthGuard = require('../authguards/admin-auth-guard');
const MenuValidator = require('../utils/menu-form-validator');
const fileExtractor = require('../utils/single-file-extractor');


router.post('/create-menu',adminAuthGuard.canActivate,
 fileExtractor,[
    MenuValidator.validateMenuName(),
    MenuValidator.validateMenuPrice(),
    MenuValidator.validateCategory(),
    MenuValidator.validateMenuDescription()
], adminController.createMenu);

router.get('/find-menu/:id', adminAuthGuard.canActivate, adminController.retrieveMenu);

router.get('/retrieve-all-menu', adminAuthGuard.canActivate, adminController.retrieveAllMenu);

router.put('/update-menu/:id', adminAuthGuard.canActivate, fileExtractor,[
    MenuValidator.validateMenuName(),
    MenuValidator.validateMenuPrice(),
    MenuValidator.validateCategory(),
    MenuValidator.validateMenuDescription()
], adminController.updateMenu);

router.delete('/delete-menu/:id', adminAuthGuard.canActivate, adminController.deleteOneMenu);


module.exports = router;