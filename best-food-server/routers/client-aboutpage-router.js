const express = require('express');
const adminAuthGuard = require('../authguards/admin-auth-guard');
const multiFileExtractor = require('../utils/multi-files-extractor');
const clientAboutpageController = require('../controllers/client-aboutpage-controller');
const standardValidator = require('../utils/standard-validator');

const router = express.Router();

router.post('/update-aboutpage', 
adminAuthGuard.canActivate,
multiFileExtractor,
[
standardValidator.validateAboutpageHeader(),
standardValidator.validateAboutpageQuote(),
standardValidator.validateAboutpagePhone()
],
clientAboutpageController.updateAboutpage);

router.get('/retrieve-aboutpage-attributes', clientAboutpageController.retrieveAboutpageAttributes);

module.exports  = router;