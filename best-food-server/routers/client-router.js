const express = require('express');

const router = express.Router();

const clientController = require('../controllers/client-controller');

router.get('/find-menu/:id', clientController.getOneMenu);

router.get('/retrieve-all-menu', clientController.getAllMenu);

router.post('/search-menu/', clientController.searchMenu);

router.post('/retrieve-category/', clientController.findMenuByCategory);

module.exports  = router;