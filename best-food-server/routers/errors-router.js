const express = require('express');

const router = express.Router();

const errorController = require('../controllers/errors-controller');

router.use(errorController.pageNotFound);

//router.use(errorController.serverError);

module.exports = router;