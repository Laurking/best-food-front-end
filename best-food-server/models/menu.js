const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const menuSchema = new Schema({
    name:{
        type:String, 
        required:true
    },
    price: {
        type: String, 
        required: true
    },
    description: { 
        type: String,
        required: true
    },
    category:{
        type: String,
        required: true
    },
    image: {
        type:String,
        required:true
    },
    createdBy:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref:"User"
    },
    updatedBy:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref:"User"
    },
    created_at:{
        type: Date,
        default:  Date.now,
        
    },
    updated_at:{
        type: Date,
        default:  Date.now,
    },
});

module.exports = mongoose.model('Menu', menuSchema);