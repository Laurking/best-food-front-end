const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const aboutpageSchema = new Schema({
    header:{
        type:String,
        required:true,
    },
    quote:{
        type:String,
        required:true,
    },
    phone:{
        type:Number,
        required:true,
    },
    topLeft: {
        type:String,
        required:true
    },
    topRight: {
        type:String,
        required:true
    },
    bottomLeft: {
        type:String,
        required:true
    },
    bottomCenter: {
        type:String,
        required:true
    },
    bottomRight: {
        type:String,
        required:true
    }
});

module.exports = mongoose.model('Aboutpage', aboutpageSchema);