const mongoose = require('mongoose');
const Bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstname:{
        type:String, 
        required:false,
        default:null
    },
    lastname:{   
        type:String, 
        required:false,
        default:null
    },
    email: {
        type: String, 
        unique:true,
        required: true, 
    },
    birthDate:{
      type: Date,
      required:false,
      default:null
    },
    password: { 
        type: String,
        required: true
    },
    active:{
        type:Boolean,
        required:true,
        default:true
    },
    userRole:{
        type:String,
        required:true,
        default:"Owner"
    },
    resetPasswordToken: {
        value:{
            type: String,
            required: false,
            default:null
        },
        expiresAt: {
            type: Date,
            required: false,
            default:null
        }
    },
    created_at: {type:Date, default:Date.now},
    updated_at: {type:Date, default:Date.now},
    passwordUpdated_at: {type:Date, required:true, default:Date.now}
});

userSchema.methods.comparePassword = function(password, callback) {
    Bcrypt.compare(password, this.password, function(err, isMatch) {
      if (err) return callback(err);
      callback(null, isMatch);
    });
  };

module.exports = mongoose.model('User', userSchema);
