const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const homepageSchema = new Schema({
    header:{
        type:String,
        required:true,
    },
    quote:{
        type:String,
        required:true,
    },
    image: {
        type:String,
        required:true
    }
});

module.exports = mongoose.model('Homepage', homepageSchema);