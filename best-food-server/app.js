// core library imports 
const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const mongoose = require('mongoose');

// import routers
const adminRouter = require('./routers/admin-router');
const clientRouter = require('./routers/client-router');
const contactRouter = require('./routers/contact-router');
const clientHomepageRotuer = require('./routers/client-homepage-router');
const clientAboutpageRotuer = require('./routers/client-aboutpage-router');
const userRouter = require('./routers/user-router');
const errorRouter = require('./routers/errors-router');

// constant variables imports
const constantVariables = require('./constants/variables');

const app = express();

app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));
// app.use("/uploads", express.static(path.join("best-food-server/uploads")));

const store = new MongoDBStore({
    uri: constantVariables.dbConnectionUrl, 
    collection:'session'
})
app.use(cookieParser());
app.use(session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store
}));

// app.use((req, res, next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader(
//       "Access-Control-Allow-Headers",
//       "Origin, X-Requested-With, Content-Type, Accept, Authorization"
//     );
//     res.setHeader(
//       "Access-Control-Allow-Methods",
//       "GET, POST, PATCH, PUT, DELETE, OPTIONS"
//     );
//     next();
//   });

const corsOptions = {
    origin:'*',
    // methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    methods: "*",
    // allowedHeaders:"Origin, X-Requested-With, Content-Type, Accept, Authorization",
    allowedHeaders:"*",
    preflightContinue: false,
    optionsSuccessStatus: 200,
    withCredentials:false
}

app.use(cors(corsOptions));

app.use('/admin',adminRouter);
app.use(clientRouter);
app.use('/contact',contactRouter);
app.use('/users',userRouter);
app.use("/client",clientHomepageRotuer);
app.use("/client",clientAboutpageRotuer);
app.use(errorRouter);

mongoose.connect(constantVariables.dbConnectionUrl)
.then(connection =>{
    console.log('connected');
})
.catch(error =>{
    console.log(error);
});

module.exports = app;
