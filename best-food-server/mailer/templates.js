const constants = require('../constants/variables');
exports.ThankYouNoteFromContact = `
<div class="container" style="width: 50%; margin:.5em 0px; padding: 10px; overflow: hidden;">
<h1 style="text-align: center; font-size: 1.2em; margin:.5em 0px 3em 0px;">Thank you for contacting us</h1>
<p>Your email is important to us. Our Customer Care Center will work with you shortly.</p>
<p>Very best,</p>
<h4>
<div>Shop 24/7 Team Support</div>
<div>Email: <span style="font-size: .8em; color: blue;">shopping.shop@twentyfour.com</span></div>
<div>Phone: <span style="font-size: .8em; color: blue;">1(212) 555-5555</span></div>
</h4>
</div>
`;


exports.resetPassword = (token)=>{
	return `<h1 style="font-size: 1.2em; text-align: center;">**PLEASE DO NOT RESPOND TO THIS E-MAIL**</h1>
	<h2 style="text-align: center; color: red;">Password Reset Request Action Needed</h2>
	<div class="container" style="padding-left: 20px;">A request to reset the password associated with this email address has been received. Please click on the button below, which will connect you to our secure server. You will then be given instructions for resetting your password.
	<div class="button" style="text-align: center;"><a href='${constants.resetPasswordUri}${token}' style="width: 200px; margin: 1.5em auto; line-height: 20px; font-size: 1em; display: block; text-decoration: none; color: #fff; outline: 2px solid #3399ff; cursor: pointer; background-color: #17a2b8; border-color: #17a2b8; padding: 5px 0px 7px 0px;">Reset Password</a></div>
	<div class="info" style="padding-top: 15px;">
	Please contact our office immediately if you did not initiate this change or if you have any questions.
	</div>
	 <p>Best regards,</p>
        <h3>African American Best Food</h3>
        <address>410 Lenox Avenue<br>New York, NY 10037 <br>Phone: (212)690-8052</address>
	</div>`
};


exports.passwordDetails = (link,password) =>{
	return `<div class="new-member-container" #confirmation style="width: 100%;padding: 1em 10px;position: absolute;height: 100%;font-family: 'Courier New', Courier, monospace;animation: moveLeft 1s linear forwards;">
	<div class="wrapper">
	  <div class="contact-us-confirmation-content">
		 <h1 style="font-size: 1em;color: red;">**PLEASE DO NOT RESPOND TO THIS E-MAIL**</h1>
		<h2 style="font-size: 1.2em;">Welcome to African American Best Food!</h2>
	   <p>The owner of the African American Best Food restaurant has created a new user account for you.</p>
	<p>Please <a href='${link}'>Click here</a> or copy and paste the following link ${link} into the browser, and then log into our website using your email and the following password:  <strong>${password}</strong></p>
	<p>If you do not want to be part of African American Best Food team, please contact the owner to unsubscribe.</p>
		  <p class="signature" style="margin-top: 1em;">Best regards,</p>
		  <h3>African American Best Food</h3>
		  <address>410 Lenox Avenue<br>New York, NY 10037 <br>Phone: (212)690-8052</address>
		  
		</div>
	  </div>
	</div>`;
}