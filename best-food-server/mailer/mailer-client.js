const nodemailer = require('nodemailer');
const sendgridTransporter = require('nodemailer-sendgrid-transport');
const constants = require('../constants/variables');

const transporter = nodemailer.createTransport(sendgridTransporter({
    auth:{
        api_key: constants.sendgridAPIKey
    }
}))

module.exports = class Mailer {
    
    
    static async sendMail(sender, receiver, subject, message){
        try{
            const response =  await transporter.sendMail({
                from: sender, 
                to: receiver,
                subject: subject,
                html: message
            });
            return response;
        }catch(error){
            return error;
        }
    }
    
    
    // static sendMail(sender, receiver, subject, message){
    //     const options={
    //         from: sender, 
    //         to: receiver,
    //         subject: subject,
    //         html: message
    //     }
    //     return transporter.sendMail(options,(error, info)=>{
    //         if(error) console.log(error);
    //         else{console.log("Email send "+info.response)};
    //     });
    // }
}

