const Bcrypt = require('crypto');
var Base64 = require('base-64');
const constants = require('../constants/variables');


module.exports = class Hash{
    static generateToken(){
        return Bcrypt.randomBytes(32).toString('hex');
    }
    static encrypt(message){
        const result = Base64.encode(JSON.stringify(message));
        return result.toString();
    }
    static decrypt(message){
        const result = Base64.decode(message);
        return result;
    }
}
