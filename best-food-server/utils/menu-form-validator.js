const { body } = require('express-validator/check');
const Menu = require('../models/menu');

module.exports = class MenuValidator{
    
    static validateMenuName(){
        return body('name')
        .isLength({min:1}).withMessage('field must not be empty')
        .isLength({min:2, max:50}).withMessage('expects between 2 and 50 characters');
    }
    
    static validateMenuPrice(){
        return body('price')
        .isLength({min:1}).withMessage('field must not be empty')
        .matches(/^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/,"i")
        .withMessage('Please enter a valid price (example 9.99)')
    }

    static validateCategory(){
        const categoryies = ['Main dish','Side order','Drink','Dessert'];
        return body('category')
        .isLength({min:1}).withMessage('field must not be empty')
        .isIn(categoryies).withMessage("category does not exist");
    }
    
    static validateMenuDescription(){
        return body('description')
        .isLength({min:1}).withMessage('field must not be empty')
        .isLength({min:10, max:500}).withMessage('expects between 10 and 500 characters')
    }
}