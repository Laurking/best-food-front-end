module.exports = class Normalizer{

    constructor(string){
        this.string = string;
    }

    normalizeString(){
        this.string = this.string != null? this.string.replace(/\s+/g, ' ').trim() : '';
        return this;
    }

 
    capitalize(){
        this.string = this.string.charAt(0).toUpperCase() + this.string.slice(1);
        return this;
    }

    toLoweCase(){
        this.string = this.string.toLowerCase();
        return this;
    }

    toUpperCase(){
        this.string = this.string.toUpperCase();
        return this;
    }


    capitalizeFullName(){
        this.string = this.string.split(' ');
        this.string = this.string[0].charAt(0).toUpperCase()+this.string[0].slice(1) + ' ' + this.string[1].charAt(0).toUpperCase()+this.string[1].slice(1)
        return this;
    }
     getStringValue(){
        return this.string;
    }
}