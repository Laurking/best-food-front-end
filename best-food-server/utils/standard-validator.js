const { body } = require('express-validator/check');
const User = require('../models/user');
const Normalizer = require('./data-field-normalizer');
const Bcryt = require('bcryptjs');


module.exports = class StandardValidator{
    
    static validateFirstName(){
        return body('firstname')
        .matches(/^[\w\s]+$/i)
        .withMessage('Please provide a valid first name');
    }
    
    static validateLastName(){
        return body('lastname')
        .matches(/^[\w\s]+$/i)
        .withMessage('Please provide a valid last name');
    }
    
    static validateBirthDate(){
        return body('birthDate')
        .isLength({min:1})
        .withMessage("Please provide a valid birth date")
        .custom(( value) => {
            const date = new Date(value);
            const year = date.getUTCFullYear();
            const month = date.getMonth();
            const day = date.getDay();
            var now = parseInt(new Date().toISOString().slice(0, 10).replace(/-/g, ''));
            var dob = year * 10000 + month * 100 + day * 1; // Coerces strings to integers
            if(!((now - dob > 150000) && (now - dob < 1000000))){
                return Promise.reject('Your age must be between 15 and 100.');
            }
            return true;
        });
    }
    
    static validateEmail(){
        return body('email')
        .isLength({min:1})
        .withMessage("field cannot be empty")
        .isEmail()
        .withMessage("require valid email")
        .custom(value => {
            return User.findOne({email: new Normalizer(value).normalizeString().toLoweCase().getStringValue()})
            .then(user => {
                if (!user) {
                    return Promise.reject('No such an email in our record.');
                }
            });
        })
    }
    
    
    static validatePassword(){
        return body('password')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:8, max:20})
        .withMessage("expects 8 to 20 characters");
    }
    
    static validateConfirmPassword(){
        return  body('confirmPassword')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .custom(( value, { req }) => {
            if (value !== req.body.password) {
                return Promise.reject('Confirmation must match password');
            } 
            return true;
        });
    }
    
    static oldPasswordCheck(){
        return body('userId')
        .custom(( value, { req })=>{
            return User.findOne({_id: value})
            .then(user => {
                if (user) {
                    return Bcryt.compare(req.body.password, user.password)
                    .then(isMatch =>{
                        if(isMatch) return Promise.reject(new Error('Old passwords not accepted'));
                    });
                }
                return true;
            });
        });
    }
    
    static isOverEighteen(date) {
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDay();
        var now = parseInt(new Date().toISOString().slice(0, 10).replace(/-/g, ''));
        var dob = year * 10000 + month * 100 + day * 1; // Coerces strings to integers
        return (now - dob > 150000) && (now - dob < 1000000);
        console.log('form is over 18'+date)
    }
    
    
    // static oldPasswordCheck(){
    //     return body('userId')
    //     .custom(( value, { req })=>{
    //         return User.findOne({_id: value})
    //         .then(user => {
    //             if (user) {
    //                return Bcryt.compare(req.body.password, user.password)
    //                 .then(isMatch =>{
    //                     if(isMatch) return Promise.reject(new Error('Old passwords not accepted'));
    //                 });
    //             }
    //             return true;
    //         });
    //     });
    // }


    static validateHomepageHeader(){
        return body('header')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:10, max:50})
        .withMessage('expects between 10 and 50 characters');
    }

    static validateHomepageQuote(){
        return body('quote')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:10, max:50})
        .withMessage('expects between 10 and 50 characters');
    }

    static validateAboutpageHeader(){
        return body('header')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:10, max:50})
        .withMessage('expects between 10 and 50 characters');
    }

    static validateAboutpageQuote(){
        return body('quote')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:10, max:50})
        .withMessage('expects between 10 and 50 characters');
    }

    static validateAboutpagePhone(){
        return body('phone')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/i)
        .withMessage("Invalid phone number");
    }
    
}